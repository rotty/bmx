(decode ("smoke.bmx" Command) "smoke.chex" "smoke.expected")
(decode ("enum.bmx" Command) "enum.chex" "enum.expected")
(decode ("derive.bmx" Inst) "derive.chex" "derive.expected")
(decode ("elf.bmx" FileHeader) "elf.chex" "elf.expected")
(decode ("ip-packet.bmx" IpPacket) "ip-packet.chex" "ip-packet.expected")
