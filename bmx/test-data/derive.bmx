;; -*- scheme -*-

(root Inst
  (fields
   (op u8)
   ;; TODO: This should be an array
   (data.0 u8)
   (data.1 u8)
   (data.2 u8)))

(branch Inst.LoadImm
  (condition (= op 1))
  (derive
   (imm (bitwise-or (bitwise-shift-left data.0 16)
                    (bitwise-shift-left data.1 8)
                    data.2))))
