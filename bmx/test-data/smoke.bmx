;; -*- scheme -*-

(root Command
  (fields (tag u8 hidden)))

(branch Command.PencilDown
  (condition (= tag 0)))

(branch Command.PencilUp
  (condition (= tag 1)))

(branch Command.Turn
  (condition (= tag 2))
  (fields (angle u32)))

(branch Command.Move
  (condition (= tag 3))
  (fields (distance u32)))

(branch Command.Loop
  (condition (= tag 4))
  (fields (n-iterations u32)
          (body-len u32 hidden)
          (body (array body-len Command))))

(branch Command.Eval
  (condition (= tag 5))
  (fields (op u8 hidden)))

(branch Command.Eval.Const
  (condition (= op 0))
  (fields (value u32)))

(branch Command.Eval.Plus
  (condition (= op 1))
  (fields (args (array 2 u32))))

;; Local Variables:
;; eval: (put 'branch 'scheme-indent-function 1)
;; eval: (put 'enum 'scheme-indent-function 1)
;; End:
