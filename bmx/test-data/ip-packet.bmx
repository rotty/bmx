;; -*- scheme -*-

(root Ipv4Option
  (fields
   (copied u1)
   (class u2)
   (number u5)
   (length u8)
   (data (array (- length 1) u8))))

(root IpPacket
  (fields
   (version (u 4))))

(branch IpPacket.V4
  (condition (= version 4))
  (fields
   (ihl u4) ; minimum value is 5
   (dscp u6)
   (ecn u2)
   (total-length u16)
   (id u16)
   (flags u3)
   (fragment-offset u13)
   (ttl u8)
   (protocol u8)
   (header-checksum u16)
   (source-address u32)
   (destination-address u32)
   (options (array (- ihl 5) Ipv4Option))))

(branch IpPacket.V4.Icmp
  (condition (= protocol 1))
  (fields
   (type u8)
   (code u8)
   (checksum u16)))

(root (IcmpV4EchoData ihl total-length)
  (fields
   (identifier u16)
   (sequence-number u16)
   (data (array (- total-length (* (+ ihl 2) 4)) u8))))

(branch IpPacket.V4.Icmp.Echo
  (condition (and (= type 8) (= code 0)))
  (implies (IcmpV4EchoData ihl total-length)))

(branch IpPacket.V4.Icmp.EchoReply
  (condition (and (= type 0) (= code 0)))
  (implies (IcmpV4EchoData ihl total-length)))

(branch IpPacket.V4.Unknown
  (condition #t)
  (fields
   (payload (array (- total-length (* ihl 4)) u8))))

;; Local Variables:
;; eval: (put 'branch 'scheme-indent-function 1)
;; eval: (put 'enum 'scheme-indent-function 1)
;; End:
