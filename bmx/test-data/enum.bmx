;; -*- scheme -*-

(enum Expr
  (discriminants (op u8))
  (variants
   (Const 0 (fields (value u32)))
   (Sum 1 (fields (n-args u8 hidden)
                  (args (array n-args Expr))))
   (Product 2 (fields (n-args u8 hidden)
                      (args (array n-args Expr))))))

(enum Command
  (discriminants (tag u8))
  (variants
   (PencilDown (= tag 0))
   (PencilUp (= tag 1))
   (Turn (= tag 2)
         (fields (angle u32)))
   (Move (= tag 3)
         (fields (distance u32)))
   (Loop (= tag 4)
         (fields (n-iterations u32)
                 (body-len u32 hidden)
                 (body (array body-len Command))))
   (Eval (= tag 5)
         (fields (expr Expr)))))

;; Local Variables:
;; eval: (put 'branch 'scheme-indent-function 1)
;; eval: (put 'enum 'scheme-indent-function 1)
;; End:
