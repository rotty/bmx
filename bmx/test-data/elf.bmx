;; -*- scheme -*-

(enum Class
  (discriminants (tag u8))
  (variants
   (Bits32 1 (properties (address-width 32)))
   (Bits64 2 (properties (address-width 64)))))

(enum Endian
  (discriminants (tag u8))
  (variants
   (Little 1 (properties (endianness le)))
   (Big 2 (properties (endianness be)))))

(enum OsAbi
  (discriminants (tag u8))
  (variants (SystemV (= tag 0))))

(root Abi
  (fields
   (os OsAbi)
   (version u8)))

(enum (FileType endianness)
  (discriminants (tag (u 16 endianness)))
  (variants
   (None 0)
   (Rel 1)
   (Exec 2)
   (Dyn 3)
   (Core 4)
   (LoOs #xFE00)
   (HiOs #xFEFF)
   (LoProc #xFF00)
   (HiProc #xFFFF)))

(enum (Machine endianness)
  (discriminants (tag (u 16 endianness)))
  (variants
   (SPARC #x02)
   (x86 #x03)
   (MIPS #x08)
   (PowerPC #x14)
   (S390 #x16)
   (ARM #x28)
   (SuperH #x2A)
   (IA-64 #x32)
   (AMD64 #x3E)
   (AArch64 #xB7)
   (RISC-V #xF3)))

(root FileHeader
  (fields
   (magic (array 4 u8) (= "\x7F;ELF") hidden)
   (class Class)
   (endianness Endian)
   (version u8 (= 1))
   (abi Abi)
   (pad (array 7 u8) hidden) ;; Needs fixed value?
   (let ((endianness (type-property endianness 'endianness))
         (address-width (type-property class 'address-width)))
     (type (FileType endianness))
     (machine (Machine endianness))
     (version (u 32 endianness) (= 1) hidden)
     (entry (u address-width endianness))
     (program-header-offset (u address-width endianness))
     (section-header-offset (u address-width endianness))
     (flags (u 32 endianness))
     (elf-header-size (u 16 endianness))
     (program-header-entry-size (u 16 endianness))
     (program-header-num (u 16 endianness))
     (section-header-entry-size (u 16 endianness))
     (section-header-num (u 16 endianness))
     (section-header-string-index (u 16 endianness)))))

;; Local Variables:
;; eval: (put 'branch 'scheme-indent-function 1)
;; eval: (put 'enum 'scheme-indent-function 1)
;; End:
