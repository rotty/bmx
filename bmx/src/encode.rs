use std::{
    collections::{BTreeMap, HashMap},
    convert::Infallible,
    fmt::Display,
};

use bitvec::{
    field::BitField,
    prelude::{self as bv},
    view::BitView,
};
use thiserror::Error;

use crate::{
    decode::CowValue,
    eval::{self, eval_type_expr, Environment},
    forest::{self, Forest},
    resolve::SynEnv,
    type_registry::{TypeId, TypeRegistry},
    Endianness, Ident, Value,
};

#[derive(Debug, Clone, Copy, Hash, PartialEq, Eq)]
pub struct NodeId(usize);

pub type TypeExpr = crate::TypeExpr<NodeId, Expr>;
pub type Expr = crate::Expr<Ident, NodeId>;
pub type Type = crate::Type<TypeId, Named>;

#[derive(Debug, Clone, PartialEq)]
pub struct Named {
    fields: BTreeMap<Ident, Value<Named>>,
}

impl Display for Named {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str("(")?;
        for (idx, (name, value)) in self.fields.iter().enumerate() {
            write!(f, "({name} {value})")?;
            if idx + 1 < self.fields.len() {
                f.write_str(" ")?;
            }
        }
        f.write_str(")")?;
        Ok(())
    }
}

impl Value<Named> {
    fn to_sexp(&self) -> lexpr::Value {
        use Value::*;
        match self {
            Bool(b) => (*b).into(),
            U32(n) => (*n).into(),
            U64(n) => (*n).into(),
            I32(n) => (*n).into(),
            I64(n) => (*n).into(),
            Endian(e) => lexpr::Value::symbol(e.name()),
            Array(elts) => lexpr::Value::list(elts.iter().map(|elt| elt.to_sexp())),
            Shaped(named) => lexpr::Value::list(
                named
                    .fields
                    .iter()
                    .map(|(k, v)| lexpr::Cons::new(k.as_str(), v.to_sexp())),
            ),
        }
    }
}

#[derive(Debug)]
pub struct Field {
    name: Ident,
    ty: TypeExpr,
}

impl Field {
    pub fn new(name: Ident, ty: TypeExpr) -> Self {
        Field { name, ty }
    }
}

#[derive(Debug)]
pub(crate) struct Node {
    fields: Vec<Field>,
}

#[derive(Debug)]
pub struct Encoder {
    fields: Vec<Field>,
    nodes: Vec<Node>,
}

#[derive(Debug)]
pub struct ResolveEnv<'a> {
    pub(crate) forest: &'a Forest,
    pub(crate) ctx: &'a mut ResolveContext,
}

#[derive(Debug, Default)]
pub struct ResolveContext {
    pub(crate) roots: HashMap<forest::NodeId, NodeId>,
    pub(crate) nodes: Vec<Node>,
}

impl ResolveContext {
    pub fn new() -> Self {
        Self::default()
    }

    pub fn include<E, F>(&mut self, root_id: forest::NodeId, f: F) -> Result<NodeId, E>
    where
        F: FnOnce(&mut Self) -> Result<NodeId, E>,
    {
        if let Some(node_id) = self.roots.get(&root_id) {
            Ok(*node_id)
        } else {
            let node_id = f(self)?;
            self.roots.insert(root_id, node_id);
            Ok(node_id)
        }
    }
    fn push_node(&mut self, node: Node) -> NodeId {
        let id = self.nodes.len();
        self.nodes.push(node);
        NodeId(id)
    }
}

impl<'a> ResolveEnv<'a> {
    pub fn new(forest: &'a Forest, ctx: &'a mut ResolveContext) -> Self {
        ResolveEnv { forest, ctx }
    }
}

impl<'a> ResolveEnv<'a> {}

impl<'a> SynEnv for ResolveEnv<'a> {
    type InputFieldId = Ident;
    type InputNodeId = forest::NodeId;

    type FieldId = Ident;
    type NodeId = NodeId;

    type Error = Infallible;

    fn resolve_node(&mut self, node_id: &Self::InputNodeId) -> Result<Self::NodeId, Self::Error> {
        let forest = self.forest;
        let node_id = self.ctx.include(*node_id, |ctx| -> Result<_, Infallible> {
            let mut fields = Vec::new();
            let forest_node = &forest[*node_id];
            let mut env = ResolveEnv { forest, ctx };
            forest.push_encoder_fields(forest_node, &mut fields, &mut env);
            Ok(ctx.push_node(Node { fields }))
        })?;
        Ok(node_id)
    }

    fn resolve_field(&mut self, name: &Self::InputFieldId) -> Result<Self::FieldId, Self::Error> {
        Ok(name.clone())
    }
}

#[derive(Debug, Error)]
pub enum Error {
    #[error("missing field value '{name}'")]
    MissingFieldValue { name: Ident },
    #[error("no such field '{name}'")]
    NoSuchField { name: Ident },
    #[error("value outside of domain")]
    ValueOutOfDomain,
    #[error("evaluation error")]
    Eval(#[from] eval::Error),
}

#[derive(Debug)]
struct EncodeEnv<'a> {
    field_values: &'a HashMap<Ident, lexpr::Value>,
    nodes: &'a [Node],
}

static ZERO_PAD: [u8; 32] = [0u8; 32];

impl<'a> EncodeEnv<'a> {
    fn encode_as(
        &self,
        ty: &Type,
        value: &Value<Named>,
        buf: &mut bv::BitVec<u8, bv::Msb0>,
        types: &mut TypeRegistry<NodeId, Named>,
    ) -> Result<(), Error> {
        match ty {
            Type::U(width, endianness) => {
                let width = *width;
                let n = value.as_u64().ok_or(Error::ValueOutOfDomain)?;
                let required_width = 64 - n.leading_zeros();
                if required_width > u32::from(width) {
                    return Err(Error::ValueOutOfDomain);
                }
                match endianness {
                    Endianness::Little => {
                        let bytes = n.to_le_bytes();
                        let n_bits = std::cmp::min(64, width);
                        buf.extend_from_bitslice(
                            &bytes.view_bits::<bv::Msb0>()[0..usize::from(n_bits)],
                        );
                        if width > 64 {
                            let n_padding_bits = usize::from(width - 64);
                            buf.extend_from_bitslice(
                                &ZERO_PAD.view_bits::<bv::Msb0>()[0..n_padding_bits],
                            );
                        }
                    }
                    Endianness::Big => {
                        let width = if width > 64 {
                            let n_padding_bits = usize::from(width - 64);
                            buf.extend_from_bitslice(
                                &ZERO_PAD.view_bits::<bv::Msb0>()[0..n_padding_bits],
                            );
                            64
                        } else {
                            width
                        };
                        let mut bytes = [0_u8; 8];
                        let bits = bytes.view_bits_mut::<bv::Msb0>();
                        bits[0..usize::from(width)].store_be(n);
                        buf.extend_from_bitslice(&bits[0..usize::from(width)]);
                    }
                    Endianness::Native => todo!(),
                }
                Ok(())
            }
            Type::I(width, endianness) => {
                let width = *width;
                let n = value.as_i64().ok_or(Error::ValueOutOfDomain)?;
                let required_width = if n >= 0 {
                    64 - n.leading_zeros() + 1
                } else {
                    64 - n.leading_ones() + 1
                };
                if required_width > u32::from(width) {
                    return Err(Error::ValueOutOfDomain);
                }
                match endianness {
                    Endianness::Little => {
                        let bytes = n.to_le_bytes();
                        let n_bits = std::cmp::min(64, width);
                        buf.extend_from_bitslice(
                            &bytes.view_bits::<bv::Msb0>()[0..usize::from(n_bits)],
                        );
                        if width > 64 {
                            let n_padding_bits = usize::from(width - 64);
                            buf.extend_from_bitslice(
                                &ZERO_PAD.view_bits::<bv::Msb0>()[0..n_padding_bits],
                            );
                        }
                    }
                    Endianness::Big => {
                        let width = if width > 64 {
                            let n_padding_bits = usize::from(width - 64);
                            buf.extend_from_bitslice(
                                &ZERO_PAD.view_bits::<bv::Msb0>()[0..n_padding_bits],
                            );
                            64
                        } else {
                            width
                        };
                        let bytes = n.to_be_bytes();
                        buf.extend_from_bitslice(
                            &bytes.view_bits::<bv::Msb0>()[64 - usize::from(width)..64],
                        );
                    }
                    Endianness::Native => todo!(),
                }
                Ok(())
            }
            Type::ArrayN(n, et) => match value {
                Value::Array(elements) => {
                    if elements.len() != *n {
                        return Err(Error::ValueOutOfDomain);
                    }
                    for element in elements {
                        self.encode_as(et, element, buf, types)?;
                    }
                    Ok(())
                }
                _ => Err(Error::ValueOutOfDomain),
            },
            Type::ArrayS(sentinel, et) => match value {
                Value::Array(elements) => {
                    for element in elements {
                        self.encode_as(et, element, buf, types)?;
                    }
                    self.encode_as(et, sentinel, buf, types)?;
                    Ok(())
                }
                _ => Err(Error::ValueOutOfDomain),
            },
            Type::ArrayV(et) => match value {
                Value::Array(elements) => {
                    for element in elements {
                        self.encode_as(et, element, buf, types)?;
                    }
                    Ok(())
                }
                _ => Err(Error::ValueOutOfDomain),
            },
            Type::Ident(type_id) => {
                let (node_id, _args) = &types[*type_id];
                match value {
                    Value::Shaped(named) => {
                        let node = &self.nodes[node_id.0];
                        // TODO: this conversion is quite unfortunate from a
                        // performance POV
                        let field_values = named
                            .fields
                            .iter()
                            .map(|(k, v)| (k.to_owned(), v.to_sexp()))
                            .collect();
                        let env = EncodeEnv {
                            field_values: &field_values,
                            nodes: self.nodes,
                        };
                        for field in &node.fields {
                            let ty = eval_type_expr(&field.ty, &env, types)?;
                            let value = named
                                .fields
                                .get(&field.name)
                                .ok_or(Error::ValueOutOfDomain)?;
                            self.encode_as(&ty, value, buf, types)?;
                        }
                        Ok(())
                    }
                    _ => Err(Error::ValueOutOfDomain),
                }
            }
        }
    }
}

impl<'a> Environment for EncodeEnv<'a> {
    type NodeId = NodeId;
    type FieldId = Ident;
    type Shaped = Named;

    fn lookup_field(&self, id: &Self::FieldId) -> eval::Result<CowValue<'_, Named>> {
        let value = &self.field_values[id];
        Ok(CowValue::Syntactic(value))
    }

    fn lookup_param(&self, _idx: usize) -> eval::Result<CowValue<'_, Named>> {
        todo!()
    }

    fn lookup_property(
        &self,
        _r: &Self::FieldId,
        _name: &str,
    ) -> eval::Result<CowValue<'_, Self::Shaped>> {
        todo!()
    }

    fn make_shaped(
        &self,
        type_id: TypeId,
        fields: HashMap<&str, CowValue<'_, Self::Shaped>>,
        types: &TypeRegistry<Self::NodeId, Self::Shaped>,
    ) -> Self::Shaped {
        let (node_id, _) = types[type_id];
        let node = &self.nodes[node_id.0];
        let named = node
            .fields
            .iter()
            .map(|field| {
                let field_value = fields
                    .get(field.name.as_str())
                    .expect("TODO: missing field");
                (field.name.clone(), field_value.to_owned())
            })
            .collect();
        Named { fields: named }
    }
    fn cmp_eq(&self, v1: &CowValue<'_, Self::Shaped>, v2: &CowValue<'_, Self::Shaped>) -> bool {
        todo!("compare values {:?} {:?}", v1, v2)
    }
}

impl Encoder {
    pub(crate) fn new(fields: Vec<Field>, nodes: Vec<Node>) -> Self {
        Encoder { fields, nodes }
    }

    pub fn encode(&self, field_values: &HashMap<Ident, lexpr::Value>) -> Result<Vec<u8>, Error> {
        let mut types = TypeRegistry::new();
        let env = EncodeEnv {
            field_values,
            nodes: &self.nodes,
        };
        let mut buf = bv::BitVec::<u8, bv::Msb0>::new();
        for field in &self.fields {
            let value = field_values
                .get(&field.name)
                .ok_or_else(|| Error::MissingFieldValue {
                    name: field.name.clone(),
                })?;
            let ty = eval_type_expr(&field.ty, &env, &mut types)?;
            env.encode_as(
                &ty,
                &ty.convert_from(value, &env, &types)
                    .ok_or(Error::ValueOutOfDomain)?,
                &mut buf,
                &mut types,
            )?;
        }
        Ok(buf.into_vec())
    }
}
