use std::{collections::HashMap, fmt, iter::FromIterator};

pub use crate::syn::Expr;
use crate::DerivedField;

pub type Field = crate::Field<Expr>;

#[derive(Debug, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
pub struct Ident(String);

impl Ident {
    pub fn as_str(&self) -> &str {
        &self.0
    }
}

impl<'a> From<&'a str> for Ident {
    fn from(s: &str) -> Self {
        Ident(s.into())
    }
}

impl From<String> for Ident {
    fn from(s: String) -> Self {
        Ident(s)
    }
}

impl<'a> From<&'a String> for Ident {
    fn from(s: &'a String) -> Self {
        Ident(s.into())
    }
}

impl fmt::Display for Ident {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.as_str())
    }
}

#[derive(Debug, Clone)]
pub struct Path(Vec<Ident>);

impl Path {
    pub fn len(&self) -> usize {
        self.0.len()
    }

    pub fn is_empty(&self) -> bool {
        self.0.is_empty()
    }

    pub fn split_branch(self) -> Result<(Ident, Path, Ident), Self> {
        if self.len() >= 2 {
            let mut components = self.0;
            let name = components.pop().unwrap();
            let root = components.remove(0);
            Ok((root, Path(components), name))
        } else {
            Err(self)
        }
    }
}

impl<'a> FromIterator<&'a Ident> for Path {
    fn from_iter<T>(iter: T) -> Path
    where
        T: IntoIterator<Item = &'a Ident>,
    {
        Path(iter.into_iter().cloned().collect())
    }
}

impl fmt::Display for Path {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        for (i, ident) in self.0.iter().enumerate() {
            write!(f, "{}", ident)?;
            if i + 1 < self.0.len() {
                f.write_str(".")?;
            }
        }
        Ok(())
    }
}

impl<'a> From<&'a str> for Path {
    fn from(s: &'a str) -> Self {
        Path(s.split('.').map(|s| s.into()).collect())
    }
}

impl<'a> IntoIterator for &'a Path {
    type IntoIter = std::slice::Iter<'a, Ident>;
    type Item = &'a Ident;

    fn into_iter(self) -> Self::IntoIter {
        self.0.iter()
    }
}

impl IntoIterator for Path {
    type IntoIter = std::vec::IntoIter<Ident>;
    type Item = Ident;

    fn into_iter(self) -> Self::IntoIter {
        self.0.into_iter()
    }
}

#[derive(Debug, Clone)]
pub struct Root {
    pub name: Ident,
    pub description: Option<String>,
    pub fields: Vec<Field>,
    pub derive: Vec<DerivedField<Expr>>,
}

#[derive(Debug, Clone)]
pub struct Branch {
    pub root: Ident,
    pub path: Path,
    pub name: Ident,
    pub description: Option<String>,
    pub condition: Expr,
    pub implies: Vec<Expr>,
    pub fields: Vec<Field>,
    pub derive: Vec<DerivedField<Expr>>,
    pub properties: HashMap<String, lexpr::Value>,
}

#[derive(Debug, Clone)]
pub struct Enum {
    pub name: Ident,
    pub description: Option<String>,
    pub discriminants: Vec<Field>,
    pub variants: Vec<Variant>,
}

#[derive(Debug, Clone)]
pub struct Variant {
    pub name: Ident,
    pub condition: Expr,
    pub fields: Vec<Field>,
    pub properties: HashMap<String, lexpr::Value>,
}
