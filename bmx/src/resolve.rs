use crate::{Expr, NodeApp, TypeExpr};

type InputTypeExpr<E> = TypeExpr<
    <E as SynEnv>::InputNodeId,
    Expr<<E as SynEnv>::InputFieldId, <E as SynEnv>::InputNodeId>,
>;
type EnvExpr<E> = Expr<<E as SynEnv>::FieldId, <E as SynEnv>::NodeId>;
type EnvTypeExpr<E> =
    TypeExpr<<E as SynEnv>::NodeId, Expr<<E as SynEnv>::FieldId, <E as SynEnv>::NodeId>>;

pub trait SynEnv {
    type InputFieldId: Clone;
    type InputNodeId: Clone;

    type FieldId: Clone;
    type NodeId: Clone;

    type Error;

    fn resolve_field(&mut self, r: &Self::InputFieldId) -> Result<Self::FieldId, Self::Error>;

    fn resolve_node(&mut self, r: &Self::InputNodeId) -> Result<Self::NodeId, Self::Error>;

    fn resolve_exprs(
        &mut self,
        exprs: &[Expr<Self::InputFieldId, Self::InputNodeId>],
    ) -> Result<Vec<EnvExpr<Self>>, Self::Error> {
        exprs
            .iter()
            .map(|expr| self.resolve_expr(expr))
            .collect::<Result<_, _>>()
    }

    fn resolve_type_expr(
        &mut self,
        te: &InputTypeExpr<Self>,
    ) -> Result<EnvTypeExpr<Self>, Self::Error> {
        use crate::TypeExpr::*;
        let te: EnvTypeExpr<Self> = match te {
            I(width, endianness) => I(self.resolve_expr(width)?, self.resolve_expr(endianness)?),
            U(width, endianness) => U(self.resolve_expr(width)?, self.resolve_expr(endianness)?),
            ArrayN(n, et) => {
                let et = self.resolve_type_expr(et)?;
                ArrayN(self.resolve_expr(n)?, Box::new(et))
            }
            ArrayV(et) => {
                let et = self.resolve_type_expr(et)?;
                ArrayV(Box::new(et))
            }
            ArrayS(sentinel, et) => {
                let et = self.resolve_type_expr(et)?;
                ArrayS(self.resolve_expr(sentinel)?, Box::new(et))
            }
            App(app) => App(resolve_app(app, self)?),
        };
        Ok(te)
    }

    fn resolve_expr(
        &mut self,
        expr: &Expr<Self::InputFieldId, Self::InputNodeId>,
    ) -> Result<EnvExpr<Self>, Self::Error> {
        use crate::Expr::*;
        let resolve_n = |args: &[Expr<Self::InputFieldId, Self::InputNodeId>],
                         env: &mut Self|
         -> Result<Vec<EnvExpr<Self>>, Self::Error> {
            args.iter().map(|arg| env.resolve_expr(arg)).collect()
        };
        match expr {
            ParamRef(idx) => Ok(ParamRef(*idx)),
            FieldRef(field_name) => Ok(FieldRef(self.resolve_field(field_name)?)),
            Apply(proc, args) => Ok(Apply(*proc, resolve_n(args, self)?)),
            Construct(op, args, named_args) => {
                let op = self.resolve_type_expr(op)?;
                let args = resolve_n(args, self)?;
                let named_args = named_args
                    .iter()
                    .map(|(field_name, expr)| {
                        self.resolve_expr(expr)
                            .map(|expr| (field_name.clone(), expr))
                    })
                    .collect::<Result<_, _>>()?;
                Ok(Construct(Box::new(op), args, named_args))
            }
            Const(c) => Ok(Const(c.clone())),
        }
    }
}

type EnvNodeApp<E> =
    NodeApp<<E as SynEnv>::NodeId, Expr<<E as SynEnv>::FieldId, <E as SynEnv>::NodeId>>;

pub fn resolve_app<E: SynEnv + ?Sized>(
    app: &NodeApp<E::InputNodeId, Expr<E::InputFieldId, E::InputNodeId>>,
    env: &mut E,
) -> Result<EnvNodeApp<E>, E::Error> {
    let node_id = env.resolve_node(&app.id)?;
    Ok(NodeApp {
        id: node_id,
        args: app
            .args
            .iter()
            .map(|arg| env.resolve_expr(arg))
            .collect::<Result<_, _>>()?,
    })
}
