use std::{collections::HashMap, convert::TryFrom, fmt::Display, mem};

use bitvec::{field::BitField, order::Msb0, view::BitView};
use im_rc::Vector;
use log::debug;
use thiserror::Error;

use crate::{
    eval::{self, eval_app, eval_expr, eval_type_expr, Environment},
    forest::NodeId,
    resolve::SynEnv,
    type_registry::{TypeId, TypeRegistry},
    Endianness, Ident, Shaped, Value,
};

pub type Expr = crate::Expr<usize, ExecNodeId>;
pub type TypeExpr = crate::TypeExpr<ExecNodeId, Expr>;
pub type Type = crate::Type<TypeId, Shaped>;
pub type NodeApp = crate::NodeApp<ExecNodeId, Expr>;

pub type Result<T> = std::result::Result<T, DecodeError>;

#[derive(Debug, Default)]
pub struct Tree {
    nodes: Vec<ExecNode>,
}

impl Tree {
    fn push_node(&mut self, node: ExecNode) -> ExecNodeId {
        let id = self.nodes.len();
        self.nodes.push(node);
        ExecNodeId(id)
    }

    pub fn follow<'a>(&'a self, node: &'a ExecNode, branch_ids: &'a [usize]) -> BranchIter<'a> {
        BranchIter { node, branch_ids }
    }
}

#[derive(Debug, Clone)]
pub enum CowValue<'a, S> {
    Syntactic(&'a lexpr::Value), // TODO: include span information
    Decoded(&'a Value<S>),
    Owned(Value<S>),
}

impl<'a, S> Display for CowValue<'a, S>
where
    S: Display,
{
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        use CowValue::*;
        match self {
            Syntactic(v) => v.fmt(f),
            Decoded(v) => v.fmt(f),
            Owned(v) => v.fmt(f),
        }
    }
}

impl<'a, V> PartialEq for CowValue<'a, V>
where
    V: PartialEq<V>,
    V: PartialEq<lexpr::Value>,
{
    fn eq(&self, other: &CowValue<'_, V>) -> bool {
        use CowValue::*;
        match (self, other) {
            (Syntactic(v1), Syntactic(v2)) => v1 == v2,
            (Syntactic(v1), Decoded(v2)) => v1 == v2,
            (Syntactic(v1), Owned(v2)) => *v1 == v2,
            (Decoded(v1), Decoded(v2)) => v1 == v2,
            (Decoded(v1), Owned(v2)) => *v1 == v2,
            (Decoded(v1), Syntactic(v2)) => v1 == v2,
            (Owned(v1), Owned(v2)) => v1 == v2,
            (Owned(v1), Decoded(v2)) => v1 == *v2,
            (Owned(v1), Syntactic(v2)) => v1 == *v2,
        }
    }
}

impl<'a, S> From<Value<S>> for CowValue<'a, S> {
    fn from(v: Value<S>) -> Self {
        CowValue::Owned(v)
    }
}

impl<'a, S> CowValue<'a, S> {
    pub fn is_true(&self) -> bool {
        use CowValue::*;
        match self {
            Syntactic(v) => !matches!(v, lexpr::Value::Bool(false)),
            Decoded(v) => v.is_true(),
            Owned(v) => v.is_true(),
        }
    }

    pub fn as_u64(&self) -> Option<u64> {
        use CowValue::*;
        match self {
            Syntactic(value) => value.as_u64(),
            Decoded(value) => value.as_u64(),
            Owned(value) => value.as_u64(),
        }
    }

    pub fn as_endianness(&self) -> Option<Endianness> {
        use CowValue::*;
        match self {
            Syntactic(value) => match *value {
                lexpr::Value::Symbol(sym) => match sym.as_ref() {
                    "be" => Some(Endianness::Big),
                    "le" => Some(Endianness::Little),
                    "ne" => Some(Endianness::Native),
                    _ => None,
                },
                _ => None,
            },
            Decoded(value) => match value {
                Value::Endian(endianness) => Some(*endianness),
                _ => None,
            },
            Owned(_) => None,
        }
    }

    pub fn as_symbol(&self) -> Option<&str> {
        use CowValue::*;
        match self {
            Syntactic(lexpr::Value::Symbol(sym)) => Some(sym.as_ref()),
            _ => None,
        }
    }

    pub fn convert_to<E>(
        self,
        ty: &crate::Type<TypeId, S>,
        env: &E,
        types: &TypeRegistry<E::NodeId, S>,
    ) -> Option<Value<S>>
    where
        Value<S>: Clone,
        E: Environment<Shaped = S>,
    {
        use CowValue::*;
        match self {
            Syntactic(value) => ty.convert_from(value, env, types),
            Decoded(value) => Some(value.clone()),
            Owned(value) => Some(value),
        }
    }

    pub fn into_owned(self) -> Value<S>
    where
        Value<S>: Clone,
    {
        use CowValue::*;
        match self {
            Syntactic(value) => syntactic_to_decoded(value),
            Decoded(value) => value.clone(),
            Owned(value) => value,
        }
    }

    pub fn to_owned(&self) -> Value<S>
    where
        Value<S>: Clone,
    {
        use CowValue::*;
        match self {
            Syntactic(value) => syntactic_to_decoded(value),
            Decoded(value) => (*value).clone(),
            Owned(value) => value.clone(),
        }
    }
}

fn syntactic_to_decoded<S>(value: &lexpr::Value) -> Value<S> {
    match value {
        lexpr::Value::Symbol(name) => match name.as_ref() {
            "be" => Value::Endian(Endianness::Big),
            "le" => Value::Endian(Endianness::Little),
            "ne" => Value::Endian(Endianness::Native),
            _ => todo!("use of symbol as decoded value: {:?}", value),
        },
        lexpr::Value::Number(n) => {
            if let Some(n) = n.as_u64() {
                if let Ok(n) = u32::try_from(n) {
                    Value::U32(n)
                } else {
                    Value::U64(n)
                }
            } else if let Some(n) = n.as_i64() {
                if let Ok(n) = i32::try_from(n) {
                    Value::I32(n)
                } else {
                    Value::I64(n)
                }
            } else {
                todo!("non-integer syntactic value as decoded value: {:?}", n)
            }
        }
        lexpr::Value::Null => Value::Array(Vec::new()),
        lexpr::Value::Cons(cell) => {
            let (elts, rest) = cell.to_vec();
            assert!(rest == lexpr::Value::Null); // FIXME: error, don't crash
            Value::Array(
                elts.into_iter()
                    .map(|elt| syntactic_to_decoded(&elt))
                    .collect(),
            )
        }
        value => todo!("use of syntactic value as decoded value: {:?}", value),
    }
}

impl<'a> CowValue<'a, Shaped> {
    pub fn to_sexp(&self) -> lexpr::Value {
        use CowValue::*;
        match self {
            Syntactic(value) => (*value).to_owned(),
            Decoded(value) => value.to_sexp(),
            Owned(value) => value.to_sexp(),
        }
    }
}

#[derive(Error, Debug, Clone)]
pub enum DecodeError {
    #[error("missing array sentinel")]
    MissingArraySentinel,
    #[error("trailing data in variable-length array")]
    TrailingArrayData,
    #[error("constant '{constant}' does not conform to type {ty:?}")]
    ConstantDoesNotConform { constant: lexpr::Value, ty: Type },
    #[error("evaluation error")]
    Eval(#[from] eval::Error),
}

impl<S> crate::Type<TypeId, S> {
    pub fn convert_from<E>(
        &self,
        value: &lexpr::Value,
        env: &E,
        types: &TypeRegistry<E::NodeId, S>,
    ) -> Option<Value<S>>
    where
        E: Environment<Shaped = S>,
    {
        use crate::Type::*;
        use lexpr::Value::*;
        match self {
            U(width, _) => match value {
                Bool(b) => Some(Value::U32(*b as u32)),
                Number(n) => {
                    if let Some(n) = n.as_u64() {
                        let required_width = 64 - n.leading_zeros();
                        if required_width > u32::from(*width) {
                            None
                        } else if required_width < 32 {
                            Some(Value::U32(n as u32))
                        } else {
                            Some(Value::U64(n))
                        }
                    } else {
                        None
                    }
                }
                _ => None,
            },
            I(width, _) => match value {
                Bool(b) => Some(Value::I32(*b as i32)),
                Number(n) => {
                    if let Some(n) = n.as_i64() {
                        if *width < 64 {
                            let min_value = -(1i64 << width);
                            let max_value = (1i64 << width) - 1;
                            if n >= min_value && n <= max_value {
                                Some(Value::from(n))
                            } else {
                                None
                            }
                        } else {
                            Some(Value::from(n))
                        }
                    } else {
                        None
                    }
                }
                _ => None,
            },
            ArrayN(n, et) => {
                let elements = array_from_value(value, et, env, types)?;
                if elements.len() != *n {
                    return None;
                }
                Some(Value::Array(elements))
            }
            ArrayV(et) => {
                let elements = array_from_value(value, et, env, types)?;
                Some(Value::Array(elements))
            }
            ArrayS(_sentinel, et) => {
                let elements = array_from_value(value, et, env, types)?;
                Some(Value::Array(elements))
            }
            Ident(type_id) => Some(Value::Shaped(env.make_shaped(
                *type_id,
                shaped_fields(value)?,
                types,
            ))),
        }
    }
}

fn shaped_fields<S>(value: &lexpr::Value) -> Option<HashMap<&str, CowValue<'_, S>>> {
    let mut items = value.list_iter()?;
    let mut fields = HashMap::new();
    for item in items.by_ref() {
        let (k, v) = expect_alist_item(item)?;
        fields.insert(k, v);
    }
    if items.next().is_some() {
        return None;
    }
    Some(fields)
}

fn expect_alist_item<S>(value: &lexpr::Value) -> Option<(&str, CowValue<'_, S>)> {
    match value {
        lexpr::Value::Cons(cell) => cell
            .car()
            .as_symbol()
            .map(|key| (key, CowValue::Syntactic(cell.cdr()))),
        _ => None,
    }
}

fn array_from_value<S, E>(
    value: &lexpr::Value,
    et: &crate::Type<TypeId, S>,
    env: &E,
    types: &TypeRegistry<E::NodeId, E::Shaped>,
) -> Option<Vec<Value<S>>>
where
    E: Environment<Shaped = S>,
{
    use lexpr::Value::*;
    match value {
        Bytes(bytes) => {
            let elements: Vec<_> = bytes
                .iter()
                .map(|&octet| et.convert_from(&lexpr::Value::from(octet), env, types))
                .collect::<Option<_>>()?;
            Some(elements)
        }
        String(s) => {
            let elements: Vec<_> = s
                .as_bytes()
                .iter()
                .map(|&octet| et.convert_from(&lexpr::Value::from(octet), env, types))
                .collect::<Option<_>>()?;
            Some(elements)
        }
        Cons(cell) => {
            let elements: Vec<_> = cell
                .list_iter()
                .map(|item| et.convert_from(item, env, types))
                .collect::<Option<_>>()?;
            Some(elements)
        }
        _ => None,
    }
}

#[derive(Debug)]
struct DecodeEnv<'a> {
    forest: &'a crate::Forest,
    tree: &'a Tree,
    ctx: DecodeContext,
}

impl<'a> DecodeEnv<'a> {
    pub fn new(forest: &'a crate::Forest, tree: &'a Tree, ctx: DecodeContext) -> Self {
        DecodeEnv { forest, tree, ctx }
    }

    fn decode_into(
        &mut self,
        node_id: ExecNodeId,
        data: &[u8],
        start_bit: usize,
        types: &mut TypeRegistry<ExecNodeId, Shaped>,
    ) -> Result<Option<usize>> {
        debug!("decoding {:?} at {}", node_id, start_bit);
        let node = &self.tree.nodes[node_id.0];
        let mut bit_pos = start_bit;
        let n_fields_start = self.ctx.field_values.len();
        for app in &node.implications {
            debug!(
                "considering implication {:?} of {:?} at {}",
                app, node_id, bit_pos
            );
            let type_id = eval_app(app, self, types)?;
            let (node_id, args) = &types[type_id];
            let node_id = *node_id;
            let mut params = args.clone(); // TODO: `clone` should not be needed here
            mem::swap(&mut params, &mut self.ctx.param_values);
            let result = self.decode_into(node_id, data, bit_pos, types);
            mem::swap(&mut params, &mut self.ctx.param_values);
            match result {
                Ok(Some(n_bits)) => bit_pos += n_bits,
                Ok(None) => {
                    self.ctx
                        .backoff(self.ctx.field_values.len() - n_fields_start);
                    return Ok(None);
                }
                Err(e) => {
                    self.ctx
                        .backoff(self.ctx.field_values.len() - n_fields_start);
                    return Err(e);
                }
            }
        }
        let n_implied_fields = self.ctx.field_values.len() - n_fields_start;
        if n_implied_fields > 0 {
            debug!(
                "sucessfully decoded {} implied fields for {:?}",
                n_implied_fields, node_id
            );
        }
        for (i, field) in node.fields.iter().enumerate() {
            let ty = eval_type_expr(&field.ty, self, types)?;
            debug!("decoding field {} as {:?} for {:?}", i, ty, node_id);
            if let Some((value, n_bits)) = self.decode_as(&ty, data, bit_pos, types)? {
                // TODO: constant fields could be omitted from the result
                if let Some(constant) = field.constant() {
                    let constant =
                        ty.convert_from(constant.value(), self, types)
                            .ok_or_else(|| DecodeError::ConstantDoesNotConform {
                                constant: constant.value().clone(),
                                ty: ty.clone(),
                            })?;
                    if value != constant {
                        self.ctx.backoff(n_implied_fields + i);
                        return Ok(None);
                    }
                }
                let end_bit = bit_pos + n_bits;
                debug!(
                    "successfully decoded field {} as {:?} for {:?}, value is {}",
                    i, ty, node_id, value
                );
                self.ctx.push_decoded(value, ty, bit_pos, end_bit);
                bit_pos = end_bit;
            } else {
                self.ctx.backoff(i);
                return Ok(None);
            }
        }
        for expr in &node.derived_fields {
            let value = eval_expr(expr, self, types)?.into_owned();
            debug!(
                "evaluated derived field of {:?}, {:?} -> {}",
                node_id, expr, value
            );
            self.ctx.push_derived(value);
        }

        for (branch_idx, branch) in node.branches.iter().enumerate() {
            debug!(
                "considering branch {} of {:?}, condition {:?}",
                branch_idx, node_id, branch.condition
            );
            if eval_expr(&branch.condition, self, types)?.is_true() {
                debug!(
                    "taking branch {} of {:?}, target {:?}",
                    branch_idx, node_id, branch.target
                );
                self.ctx.push_branch(branch_idx);
                if let Some(n_bits) = self.decode_into(branch.target, data, bit_pos, types)? {
                    debug!(
                        "branch {} of {:?} sucessully decoded {} bits",
                        branch_idx, node_id, n_bits
                    );
                    return Ok(Some((bit_pos + n_bits) - start_bit));
                } else {
                    debug!(
                        "branch {} of {:?} failed to produce a result, backing off",
                        branch_idx, node_id
                    );
                    self.ctx.pop_branch();
                }
            } else {
                debug!("branch {} of {:?} not taken", branch_idx, node_id);
            }
        }
        if node.branches.is_empty() {
            Ok(Some(bit_pos - start_bit))
        } else {
            self.ctx.backoff(node.fields.len());
            Ok(None)
        }
    }

    fn decode_as(
        &self,
        ty: &Type,
        data: &[u8],
        bit_pos: usize,
        types: &mut TypeRegistry<ExecNodeId, Shaped>,
    ) -> Result<Option<(Value<Shaped>, usize)>> {
        let start_octet = bit_pos / 8;
        let bit_offset = bit_pos % 8;
        match ty {
            Type::U(width, endianness) => {
                if start_octet >= data.len() {
                    return Ok(None);
                }
                let n_bits = usize::from(*width);
                let bits = data[start_octet..].view_bits::<Msb0>(); // TODO: limit slice size
                if bits.len() < bit_offset + n_bits {
                    return Ok(None);
                }
                match endianness {
                    Endianness::Big => {
                        let value = if n_bits <= 32 {
                            Value::U32(bits[bit_offset..bit_offset + n_bits].load_be())
                        } else {
                            Value::U64(bits[bit_offset..bit_offset + n_bits].load_be())
                        };
                        Ok(Some((value, n_bits)))
                    }
                    Endianness::Little => {
                        let value = if n_bits <= 32 {
                            Value::U32(bits[bit_offset..bit_offset + n_bits].load_le())
                        } else {
                            Value::U64(bits[bit_offset..bit_offset + n_bits].load_le())
                        };
                        Ok(Some((value, n_bits)))
                    }
                    Endianness::Native => {
                        let value = if n_bits <= 32 {
                            Value::U32(bits[bit_offset..bit_offset + n_bits].load())
                        } else {
                            Value::U64(bits[bit_offset..bit_offset + n_bits].load())
                        };
                        Ok(Some((value, n_bits)))
                    }
                }
            }
            Type::I(width, endianness) => {
                let n_bits = usize::from(*width);
                let bits = data[start_octet..].view_bits::<Msb0>(); // TODO: limit slice size
                if bits.len() < bit_offset + n_bits {
                    return Ok(None);
                }
                match endianness {
                    Endianness::Big => {
                        let value = if n_bits <= 32 {
                            Value::I32(bits[bit_offset..bit_offset + n_bits].load_be())
                        } else {
                            Value::I64(bits[bit_offset..bit_offset + n_bits].load_be())
                        };
                        Ok(Some((value, n_bits)))
                    }
                    Endianness::Little => {
                        let value = if n_bits <= 32 {
                            Value::I32(bits[bit_offset..bit_offset + n_bits].load_le())
                        } else {
                            Value::I64(bits[bit_offset..bit_offset + n_bits].load_le())
                        };
                        Ok(Some((value, n_bits)))
                    }
                    Endianness::Native => {
                        let value = if n_bits <= 32 {
                            Value::I32(bits[bit_offset..bit_offset + n_bits].load())
                        } else {
                            Value::I64(bits[bit_offset..bit_offset + n_bits].load())
                        };
                        Ok(Some((value, n_bits)))
                    }
                }
            }
            Type::ArrayN(n, et) => Ok(self
                .decode_array_n(*n, et, data, bit_pos, types)?
                .map(|(elements, n_bits)| (Value::Array(elements), n_bits))),
            Type::ArrayV(et) => Ok(self
                .decode_array_v(et, data, bit_pos, types)?
                .map(|(elements, n_bits)| (Value::Array(elements), n_bits))),
            Type::ArrayS(sentinel, et) => Ok(self
                .decode_array_s(et, sentinel, data, bit_pos, types)?
                .map(|(elements, n_bits)| (Value::Array(elements), n_bits))),
            Type::Ident(type_id) => {
                let (node_id, args) = &types[*type_id];
                let ctx = DecodeContext::new(*node_id, args.clone());
                let mut env = DecodeEnv::new(self.forest, self.tree, ctx);
                Ok(env
                    .decode_into(*node_id, data, bit_pos, types)?
                    .map(|n_bits| (Value::Shaped(env.ctx.into_shaped()), n_bits)))
            }
        }
    }

    fn decode_array_n(
        &self,
        n: usize,
        et: &Type,
        data: &[u8],
        start_bit: usize,
        types: &mut TypeRegistry<ExecNodeId, Shaped>,
    ) -> Result<Option<(Vec<Value<Shaped>>, usize)>> {
        let mut elts = Vec::with_capacity(n);
        let mut bit_pos = start_bit;
        for _ in 0..n {
            let (value, n_bits) = match self.decode_as(et, data, bit_pos, types)? {
                Some(result) => result,
                None => return Ok(None),
            };
            elts.push(value);
            bit_pos += n_bits;
        }
        Ok(Some((elts, bit_pos - start_bit)))
    }

    fn decode_array_v(
        &self,
        et: &Type,
        data: &[u8],
        start_bit: usize,
        types: &mut TypeRegistry<ExecNodeId, Shaped>,
    ) -> Result<Option<(Vec<Value<Shaped>>, usize)>> {
        let mut elts = Vec::new();
        let mut bit_pos = start_bit;
        let last_bit = data.len() * 8;
        while bit_pos < last_bit {
            let (value, n_bits) = match self.decode_as(et, data, bit_pos, types)? {
                Some(result) => result,
                None => return Err(DecodeError::TrailingArrayData),
            };
            elts.push(value);
            bit_pos += n_bits;
        }
        Ok(Some((elts, bit_pos - start_bit)))
    }

    fn decode_array_s(
        &self,
        et: &Type,
        sentinel: &Value<Shaped>,
        data: &[u8],
        start_bit: usize,
        types: &mut TypeRegistry<ExecNodeId, Shaped>,
    ) -> Result<Option<(Vec<Value<Shaped>>, usize)>> {
        if start_bit >= data.len() * 8 {
            return Ok(None);
        }
        let mut elts = Vec::new();
        let mut bit_pos = start_bit;
        loop {
            let (value, n_bits) = match self.decode_as(et, data, bit_pos, types)? {
                Some(result) => result,
                None => return Err(DecodeError::MissingArraySentinel),
            };
            bit_pos += n_bits;
            if &value == sentinel {
                return Ok(Some((elts, bit_pos - start_bit)));
            }
            elts.push(value);
        }
    }
}

impl<'a> Environment for DecodeEnv<'a> {
    type NodeId = ExecNodeId;
    type FieldId = usize;
    type Shaped = Shaped;

    fn lookup_field(&self, id: &Self::FieldId) -> eval::Result<CowValue<'_, Self::Shaped>> {
        let value = self.ctx.value(*id);
        Ok(CowValue::Decoded(value))
    }

    fn lookup_param(&self, idx: usize) -> eval::Result<CowValue<'_, Self::Shaped>> {
        Ok(CowValue::Decoded(&self.ctx.param_values[idx]))
    }

    fn lookup_property(
        &self,
        field_idx: &Self::FieldId,
        name: &str,
    ) -> eval::Result<CowValue<'_, Self::Shaped>> {
        let field = self.ctx.field_value(*field_idx);
        let property_value = match (field.decoded().map(DecodeInfo::ty), &field.value) {
            (Some(Type::Ident(_)), Value::Shaped(shaped)) => self
                .ctx
                .lookup_property(&shaped.trace, name, self.tree)
                .ok_or_else(|| eval::Error::MissingTypeProperty(name.into()))?,
            _ => {
                // FIXME: better error
                return Err(eval::Error::MissingTypeProperty(name.into()));
            }
        };
        Ok(CowValue::Syntactic(property_value))
    }

    fn make_shaped(
        &self,
        type_id: TypeId,
        fields: HashMap<&str, CowValue<'_, Self::Shaped>>,
        types: &TypeRegistry<Self::NodeId, Self::Shaped>,
    ) -> Self::Shaped {
        let (node_id, _) = types[type_id];
        let trace = Trace::new(node_id);
        let exec_node = &self.tree[node_id];
        let values = self
            .forest
            .fields(exec_node.node_id)
            .map(|field| {
                let field_value = fields
                    .get(field.name.as_str())
                    .expect("TODO: missing field");
                FieldValue {
                    value: field_value.to_owned(),
                    decoded: None,
                }
            })
            .collect();
        Shaped { trace, values }
    }

    fn cmp_eq(&self, v1: &CowValue<'_, Self::Shaped>, v2: &CowValue<'_, Self::Shaped>) -> bool {
        use CowValue::*;
        match (v1, v2) {
            (Syntactic(v1), Syntactic(v2)) => v1 == v2,
            (Syntactic(v1), Owned(v2)) => v1 == &v2,
            (Syntactic(v1), Decoded(v2)) => v1 == v2,
            (Owned(v1), Owned(v2)) => v1 == v2,
            (Owned(v1), Syntactic(v2)) => &v1 == v2,
            (Owned(v1), Decoded(v2)) => &v1 == v2,
            (Decoded(v1), Decoded(v2)) => v1 == v2,
            (Decoded(v1), Syntactic(v2)) => v1 == v2,
            (Decoded(v1), Owned(v2)) => v1 == &v2,
        }
    }
}

impl std::ops::Index<ExecNodeId> for Tree {
    type Output = ExecNode;

    fn index(&self, node_id: ExecNodeId) -> &Self::Output {
        &self.nodes[node_id.0]
    }
}

#[derive(Debug)]
pub struct ExecNode {
    node_id: NodeId,
    properties: HashMap<String, lexpr::Value>,
    implications: Vec<NodeApp>,
    fields: Vec<Field>,
    derived_fields: Vec<Expr>,
    field_env: FieldEnv,
    branches: Vec<Branch>,
}

#[derive(Debug)]
pub struct Field {
    ty: TypeExpr,
    constant: Option<lexpr::Datum>,
}

impl Field {
    pub fn new(ty: TypeExpr, constant: Option<lexpr::Datum>) -> Self {
        Field { ty, constant }
    }

    pub fn ty(&self) -> &TypeExpr {
        &self.ty
    }
    pub fn constant(&self) -> Option<lexpr::datum::Ref<'_>> {
        self.constant.as_ref().map(|datum| datum.as_ref())
    }
}

#[derive(Eq, PartialEq, Hash, Copy, Clone, Debug)]
pub struct ExecNodeId(usize);

impl ExecNode {
    pub fn new(
        node_id: NodeId,
        properties: HashMap<String, lexpr::Value>,
        implications: Vec<NodeApp>,
        fields: Vec<Field>,
        derived_fields: Vec<Expr>,
        field_env: FieldEnv,
    ) -> Self {
        assert!(field_env.unresolved.is_empty()); // TODO: reflect these checks in the type system
        ExecNode {
            properties,
            node_id,
            implications,
            fields,
            derived_fields,
            field_env,
            branches: Vec::new(),
        }
    }

    pub fn property(&self, name: &str) -> Option<&lexpr::Value> {
        self.properties.get(name)
    }

    pub fn node_id(&self) -> NodeId {
        self.node_id
    }

    pub fn fields(&self) -> &[Field] {
        &self.fields
    }

    pub fn branches(&self) -> &[Branch] {
        &self.branches
    }

    pub fn field_env(&self) -> &FieldEnv {
        &self.field_env
    }
}

#[derive(Debug)]
pub struct BranchIter<'a> {
    node: &'a ExecNode,
    branch_ids: &'a [usize],
}

impl<'a> BranchIter<'a> {
    pub fn is_empty(&self) -> bool {
        self.branch_ids.is_empty()
    }
}

impl<'a> Iterator for BranchIter<'a> {
    type Item = &'a Branch;

    fn next(&mut self) -> Option<Self::Item> {
        if let Some((&branch_id, branch_ids)) = self.branch_ids.split_first() {
            let branch = &self.node.branches[branch_id];
            self.branch_ids = branch_ids;
            Some(branch)
        } else {
            None
        }
    }
}

#[derive(Debug, Clone, PartialEq)]
pub struct Trace {
    pub root_id: ExecNodeId,
    pub branch_ids: Vec<usize>,
}

impl Trace {
    pub fn new(root_id: ExecNodeId) -> Self {
        Trace {
            root_id,
            branch_ids: Vec::new(),
        }
    }

    pub fn push(&mut self, branch_idx: usize) {
        self.branch_ids.push(branch_idx)
    }
}

#[derive(Debug)]
pub struct Branch {
    pub condition: Expr,
    pub target: ExecNodeId,
}

impl Branch {
    pub fn new(condition: Expr, target: ExecNodeId) -> Self {
        Branch { condition, target }
    }
}

#[derive(Debug, Default)]
pub struct PrepareContext {
    roots: HashMap<NodeId, ExecNodeId>,
    tree: Tree,
}

impl PrepareContext {
    pub fn new() -> Self {
        Default::default()
    }

    pub fn include<E, F>(&mut self, root_id: NodeId, f: F) -> std::result::Result<ExecNodeId, E>
    where
        F: FnOnce(&mut Self) -> std::result::Result<ExecNodeId, E>,
    {
        if let Some(exec_id) = self.roots.get(&root_id) {
            debug!("including existing {:?} -> {:?}", root_id, exec_id);
            Ok(*exec_id)
        } else {
            let exec_id = f(self)?;
            self.roots.insert(root_id, exec_id);
            debug!("including new {:?} -> {:?}", root_id, exec_id);
            Ok(exec_id)
        }
    }

    pub fn get(&self, node_id: ExecNodeId) -> Option<&ExecNode> {
        self.tree.nodes.get(node_id.0)
    }

    pub fn push_node(&mut self, node: ExecNode, is_root: bool) -> ExecNodeId {
        let node_id = node.node_id();
        let exec_id = self.tree.push_node(node);
        if is_root {
            self.roots.insert(node_id, exec_id);
        }
        exec_id
    }

    pub fn finish(self, root_id: ExecNodeId) -> Decoder {
        Decoder {
            root_id,
            tree: self.tree,
        }
    }

    pub fn set_branches(&mut self, node_id: ExecNodeId, branches: Vec<Branch>) {
        self.tree.nodes[node_id.0].branches = branches;
    }
}

#[derive(Debug)]
pub struct Decoder {
    root_id: ExecNodeId,
    tree: Tree,
}

impl Decoder {
    pub fn nodes(&self) -> std::slice::Iter<'_, ExecNode> {
        self.tree.nodes.iter()
    }

    pub(crate) fn tree(&self) -> &Tree {
        &self.tree
    }

    pub fn decode(
        &self,
        data: &[u8],
        start_bit: usize,
        forest: &crate::Forest,
    ) -> Result<Option<(Shaped, usize)>> {
        let ctx = DecodeContext::new(self.root_id, vec![]);
        let mut env = DecodeEnv::new(forest, &self.tree, ctx);
        let mut types = TypeRegistry::new();
        Ok(env
            .decode_into(self.root_id, data, start_bit, &mut types)?
            .map(|n_bits| (env.ctx.into_shaped(), n_bits)))
    }
}

#[derive(Debug, Clone, Default)]
pub struct FieldEnv {
    resolved: Vector<(Ident, TypeExpr)>,
    unresolved: Vec<Ident>,
}

impl FieldEnv {
    pub fn new() -> Self {
        Default::default()
    }

    pub fn push(&mut self, ident: Ident, ty: TypeExpr) {
        self.resolved.push_back((ident, ty));
    }

    pub fn push_names<'a, T>(&mut self, iter: T)
    where
        T: IntoIterator<Item = &'a Ident>,
    {
        self.unresolved.extend(iter.into_iter().cloned());
    }

    pub fn extend(&mut self, env: FieldEnv) {
        assert!(env.unresolved.is_empty());
        self.resolved.extend(env.resolved);
    }

    pub fn resolve_types<T>(&mut self, types: T)
    where
        T: IntoIterator<Item = TypeExpr>,
    {
        let mut types = types.into_iter();
        for ident in self.unresolved.drain(0..) {
            let ty = types.next().expect("inconsistemt use of FieldEnv");
            self.resolved.push_back((ident, ty));
        }
        assert!(self.unresolved.is_empty());
    }

    pub fn find_idx(&self, name: &Ident) -> Option<usize> {
        for (i, field_name) in self.unresolved.iter().enumerate().rev() {
            if field_name == name {
                return Some(self.resolved.len() + i);
            }
        }
        for (i, (field_name, _)) in self.resolved.iter().enumerate().rev() {
            if field_name == name {
                return Some(i);
            }
        }
        None
    }
}

#[derive(Error, Debug)]
#[error(transparent)]
pub struct PrepareError(#[from] InnerPrepareError);

#[derive(Error, Debug)]
enum InnerPrepareError {
    #[error("unknown field: `{0}`")]
    UnknownField(Ident),
}

#[derive(Debug)]
pub struct DecodeSynEnv<'a> {
    pub fields: &'a FieldEnv,
    pub prepared: &'a mut PrepareContext,
    pub forest: &'a crate::Forest,
}

impl<'a> SynEnv for DecodeSynEnv<'a> {
    type InputNodeId = NodeId;
    type InputFieldId = Ident;

    type FieldId = usize;
    type NodeId = ExecNodeId;

    type Error = PrepareError;

    fn resolve_node(
        &mut self,
        node_id: &Self::InputNodeId,
    ) -> std::result::Result<Self::NodeId, Self::Error> {
        let forest = self.forest;
        let exec_id = self.prepared.include(*node_id, |ctx| {
            forest.prepare_node(*node_id, *node_id, ctx, &mut FieldEnv::new())
        })?;
        Ok(exec_id)
    }

    fn resolve_field(
        &mut self,
        name: &Self::InputFieldId,
    ) -> std::result::Result<Self::FieldId, Self::Error> {
        Ok(self
            .fields
            .find_idx(name)
            .ok_or_else(|| InnerPrepareError::UnknownField(name.clone()))?)
    }
}

#[derive(Debug)]
pub struct DecodeContext {
    param_values: Vec<Value<Shaped>>,
    //type_registry: TypeRegistry<ExecNodeId, Shaped>,
    trace: Trace,
    field_values: Vec<FieldValue>,
}

#[derive(Debug, Clone, PartialEq)]
pub struct FieldValue {
    value: Value<Shaped>,
    decoded: Option<DecodeInfo>,
}

impl FieldValue {
    pub fn value(&self) -> &Value<Shaped> {
        &self.value
    }

    pub fn decoded(&self) -> Option<&DecodeInfo> {
        self.decoded.as_ref()
    }
}

#[derive(Debug, Clone, PartialEq)]
pub struct DecodeInfo {
    ty: Type,
    start_bit: usize,
    end_bit: usize,
}

impl DecodeInfo {
    pub fn ty(&self) -> &Type {
        &self.ty
    }
    pub fn start_bit(&self) -> usize {
        self.start_bit
    }
    pub fn end_bit(&self) -> usize {
        self.end_bit
    }
}

impl DecodeContext {
    fn new(root_id: ExecNodeId, param_values: Vec<Value<Shaped>>) -> Self {
        DecodeContext {
            param_values,
            field_values: Vec::new(),
            trace: Trace::new(root_id),
        }
    }

    fn field_value(&self, i: usize) -> &FieldValue {
        &self.field_values[i]
    }

    fn value(&self, i: usize) -> &Value<Shaped> {
        &self.field_values[i].value
    }

    fn backoff(&mut self, n: usize) {
        let new_len = self.field_values.len() - n;
        self.field_values.truncate(new_len);
    }

    fn push_decoded(&mut self, value: Value<Shaped>, ty: Type, start_bit: usize, end_bit: usize) {
        self.field_values.push(FieldValue {
            value,
            decoded: Some(DecodeInfo {
                ty,
                start_bit,

                end_bit,
            }),
        });
    }

    fn push_derived(&mut self, value: Value<Shaped>) {
        self.field_values.push(FieldValue {
            value,
            decoded: None,
        })
    }

    fn push_branch(&mut self, branch_id: usize) {
        self.trace.push(branch_id)
    }

    fn pop_branch(&mut self) {
        self.trace
            .branch_ids
            .pop()
            .expect("unbalanced usage of decode context");
    }

    fn lookup_property<'a>(
        &self,
        trace: &Trace,
        name: &str,
        tree: &'a Tree,
    ) -> Option<&'a lexpr::Value> {
        let root_node = &tree[trace.root_id];
        let final_branch = tree.follow(root_node, &trace.branch_ids).last().unwrap();
        let node = &tree[final_branch.target];
        node.property(name)
    }

    fn into_shaped(self) -> Shaped {
        Shaped {
            trace: self.trace,
            values: self.field_values,
        }
    }
}
