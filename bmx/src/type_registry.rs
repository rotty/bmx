use std::{collections::HashMap, hash::Hash};

use crate::Value;

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub struct TypeId(usize);

#[derive(Debug, Clone)]
pub struct TypeRegistry<N, S> {
    type_infos: Vec<(N, Vec<Value<S>>)>,
    type_ids: HashMap<N, Vec<TypeId>>,
}

impl<N, S> Default for TypeRegistry<N, S> {
    fn default() -> Self {
        TypeRegistry {
            type_infos: Vec::new(),
            type_ids: HashMap::new(),
        }
    }
}

impl<N, S> TypeRegistry<N, S> {
    pub fn new() -> Self {
        Self::default()
    }
}

impl<N, S> TypeRegistry<N, S>
where
    N: Hash + Eq + Clone,
    Value<S>: PartialEq,
{
    pub fn register_type(&mut self, node_id: N, args: Vec<Value<S>>) -> TypeId {
        use std::collections::hash_map::Entry::*;
        match self.type_ids.entry(node_id.clone()) {
            Vacant(vacant) => {
                let type_id = TypeId(self.type_infos.len());
                self.type_infos.push((node_id, args));
                vacant.insert(vec![type_id]);
                type_id
            }
            Occupied(mut occupied) => {
                for type_id in occupied.get() {
                    if args == self.type_infos[type_id.0].1 {
                        return *type_id;
                    }
                }
                let type_id = TypeId(self.type_infos.len());
                self.type_infos.push((node_id, args));
                occupied.get_mut().push(type_id);
                type_id
            }
        }
    }
    pub fn lookup_type(&self, node_id: N, args: &[Value<S>]) -> Option<TypeId> {
        self.type_ids.get(&node_id)?.iter().find_map(|&type_id| {
            if self.type_infos[type_id.0].1 == args {
                Some(type_id)
            } else {
                None
            }
        })
    }
}

impl<N, S> std::ops::Index<TypeId> for TypeRegistry<N, S> {
    type Output = (N, Vec<Value<S>>);

    fn index(&self, id: TypeId) -> &Self::Output {
        &self.type_infos[id.0]
    }
}
