use std::{collections::HashMap, fmt, iter, ops::RangeInclusive};

use log::{debug, trace};
use thiserror::Error;

use crate::{
    data::{self, Path},
    decode::{self, Decoder, ExecNode, Tree},
    encode::{self, Encoder},
    resolve::SynEnv,
    syn, DerivedField, Ident, Primitive, Shaped, Value,
};

pub type Expr = crate::Expr<Ident, NodeId>;
pub type TypeExpr = crate::TypeExpr<NodeId, Expr>;
pub type Field = crate::Field<TypeExpr>;
pub type Item<'a> = crate::Item<'a, TypeExpr, Expr>;
pub type NodeApp = crate::NodeApp<NodeId, Expr>;

#[derive(Debug)]
pub struct Node {
    properties: HashMap<String, lexpr::Value>,
    field_ids: Vec<FieldId>,
    derived_fields: Vec<DerivedField<Expr>>,
    implications: Vec<NodeApp>,
    branches: Vec<Branch>,
}

impl Node {
    fn implications(&self) -> &[NodeApp] {
        &self.implications
    }

    fn field_ids(&self) -> &[FieldId] {
        &self.field_ids
    }

    fn find_branch(&self, name: &Ident) -> Option<usize> {
        self.branches
            .iter()
            .enumerate()
            .find_map(|(i, b)| if &b.name == name { Some(i) } else { None })
    }
}

#[derive(Debug)]
struct Branch {
    name: Ident,
    target: NodeId,
    condition: Expr,
}

impl Branch {
    fn name(&self) -> &Ident {
        &self.name
    }
}

#[derive(Eq, PartialEq, Debug, Copy, Clone)]
struct FieldId(usize);

#[derive(Eq, PartialEq, Hash, Debug, Copy, Clone)]
pub struct NodeId(usize);

#[derive(Error, Debug)]
#[error(transparent)]
pub struct Error(#[from] InnerError);

impl Error {
    pub fn display<'a>(&'a self, forest: &'a Forest) -> ErrorDisplay<'a> {
        ErrorDisplay {
            error: self,
            forest,
        }
    }

    fn type_app_argument_count_mismatch(
        constructor: &Ident,
        expected: RangeInclusive<usize>,
        given: usize,
    ) -> Self {
        Error(InnerError::BadTypeApplication {
            constructor: constructor.clone(),
            error: TypeConstructError::ArgumentCountMismatch { expected, given },
        })
    }
}

// TODO: include reference to source span
#[derive(Error, Debug)]
enum InnerError {
    #[error("root `{0}` not defined")]
    UnknownRoot(Ident),
    #[error("branch `{name}` not defined below `{root_name}`")]
    UnknownBranch {
        root_name: Ident,
        root_id: NodeId,
        branch_ids: Vec<usize>,
        name: Ident,
    },
    #[error("constant used as type")]
    ConstantUsedAsType,
    // TODO: we probably want to allow type parameters
    #[error("parameters cannot be used as types yet")]
    ParameterUsedAsType,
    #[error("cannot apply type constructor `{constructor}`: {error}")]
    BadTypeApplication {
        constructor: Ident,
        error: TypeConstructError,
    },
    #[error("keyword arguments cannot be passed to primitives")]
    KeywordArgumentForPrimitive,
    #[error("invalid type name: `{0}`")]
    InvalidTypeName(Ident),
    #[error("expected type constructor")]
    ExpectedTypeConstructor,
    #[error("parameter in operator position")]
    ParamInOperatorPosition,
    #[error("constant in operator position")]
    ConstantInOperatorPosition,
}

#[derive(Error, Debug)]
pub(crate) enum TypeConstructError {
    #[error("keyword arguments unsupported")]
    KeywordArgumentsUnsupported,
    #[error("expected {expected:?} arguments, got {given}")]
    ArgumentCountMismatch {
        expected: RangeInclusive<usize>,
        given: usize,
    },
}

pub struct ErrorDisplay<'a> {
    #[allow(dead_code)] // See FIXME below
    forest: &'a Forest,
    error: &'a Error,
}

impl<'a> fmt::Display for ErrorDisplay<'a> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        use InnerError::*;
        match &self.error.0 {
            UnknownBranch {
                root_name, name, ..
            } => {
                // FIXME: display full path
                write!(f, "unknown branch below {}: {}", root_name, name)
            }
            _ => self.error.fmt(f),
        }
    }
}

#[derive(Error, Debug)]
pub enum DecoderPrepareError {
    #[error("requested root not defined")]
    UnknownRoot,
    #[error("error in name resolution: `{0}`")]
    Preparation(#[from] decode::PrepareError),
}

#[derive(Error, Debug)]
pub enum EncoderPrepareError {
    #[error("empty path")]
    EmptyPath,
    #[error("requested path not defined")]
    UnknownPath,
}

#[derive(Debug, Default)]
pub struct Forest {
    nodes: Vec<Node>,
    fields: Vec<Field>,
    roots: HashMap<Ident, NodeId>,
    root_names: Vec<Ident>, // in insertion order
}

#[derive(Debug, Clone)]
pub struct Cursor<'a> {
    forest: &'a Forest,
    tree: &'a decode::Tree,
    node: &'a ExecNode,
    node_fields: FieldIter<'a>,
    node_derived: &'a [DerivedField<Expr>],
    branch_ids: &'a [usize],
    values: &'a [decode::FieldValue],
    skip_hidden: bool,
}

impl<'a> Cursor<'a> {
    fn is_empty(&self) -> bool {
        if self.skip_hidden {
            self.clone().next().is_none()
        } else {
            self.values.is_empty()
        }
    }
}

impl<'a> Iterator for Cursor<'a> {
    type Item = (Item<'a>, &'a Value<Shaped>);

    fn next(&mut self) -> Option<Self::Item> {
        loop {
            if let Some(field) = self.node_fields.next() {
                let (item, rest) = self.values.split_first().expect("invalid cursor");
                self.values = rest;
                if self.skip_hidden && field.hidden {
                    continue;
                }
                return Some((Item::Decoded(field), item.value()));
            } else if let Some((field, derived_rest)) = self.node_derived.split_first() {
                let (item, rest) = self.values.split_first().expect("invalid cursor");
                self.values = rest;
                self.node_derived = derived_rest;
                return Some((Item::Derived(field), item.value()));
            } else if let Some((branch_id, branch_ids)) = self.branch_ids.split_first() {
                self.node = &self.tree[self.node.branches()[*branch_id].target];
                self.node_fields = self.forest.fields(self.node.node_id());
                self.node_derived = &self.forest[self.node.node_id()].derived_fields;
                self.branch_ids = branch_ids;
            } else {
                return None;
            }
        }
    }
}

#[derive(Debug)]
pub struct Record<'a> {
    forest: &'a Forest,
    tree: &'a decode::Tree,
    node: &'a ExecNode,
    branch_ids: &'a [usize],
    values: &'a [decode::FieldValue],
}

impl<'a> Record<'a> {
    pub fn new(shaped: &'a Shaped, forest: &'a Forest, tree: &'a Tree) -> Self {
        Record {
            forest,
            tree,
            node: &tree[shaped.root_id()],
            branch_ids: shaped.branch_ids(),
            values: shaped.field_values(),
        }
    }

    fn cursor(&'a self, skip_hidden: bool) -> Cursor<'a> {
        Cursor {
            forest: self.forest,
            tree: self.tree,
            node: self.node,
            node_fields: self.forest.fields(self.node.node_id()),
            node_derived: &self.forest[self.node.node_id()].derived_fields,
            branch_ids: self.branch_ids,
            values: self.values,
            skip_hidden,
        }
    }
}

impl<'a> fmt::Display for Record<'a> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let mut branches = self.forest.follow(self.node.node_id(), self.branch_ids);
        let anonymous = branches.is_empty();
        while let Some(branch) = branches.next() {
            f.write_str(branch.name().as_str())?;
            if !branches.is_empty() {
                f.write_str(".")?;
            }
        }
        let mut cursor = self.cursor(true);
        if cursor.is_empty() {
            return Ok(());
        }
        if anonymous {
            f.write_str("{ ")?;
        } else {
            f.write_str(" { ")?;
        }
        while let Some((field, value)) = cursor.next() {
            write!(
                f,
                "{}: {}",
                field.name(),
                value.display(self.forest, self.tree)
            )?;
            if !cursor.is_empty() {
                f.write_str(", ")?;
            }
        }
        f.write_str(" }")
    }
}

impl<'a> From<Record<'a>> for lexpr::Value {
    fn from(record: Record<'a>) -> Self {
        lexpr::Value::from(&record)
    }
}

impl<'a> From<&Record<'a>> for lexpr::Value {
    fn from(record: &Record<'a>) -> Self {
        use lexpr::Value;
        let branch_names: Vec<_> = record
            .forest
            .follow(record.node.node_id(), record.branch_ids)
            .map(|branch| branch.name().as_str())
            .collect();
        let fields = record.cursor(true).map(|(field, value)| {
            Value::cons(
                Value::symbol(field.name().as_str()),
                value.to_sexp_named(record.forest, record.tree),
            )
        });
        if branch_names.is_empty() {
            Value::list(fields)
        } else {
            let name = branch_names.join(".");
            Value::list(iter::once(Value::symbol(name)).chain(fields))
        }
    }
}

/// Iterator for the fields of a node, including implied ones.
#[derive(Debug, Clone)]
pub(crate) struct FieldIter<'a> {
    forest: &'a Forest,
    implied_iter: Option<Box<FieldIter<'a>>>,
    implications: std::slice::Iter<'a, NodeApp>,
    field_ids: std::slice::Iter<'a, FieldId>,
}

impl<'a> FieldIter<'a> {
    fn new(forest: &'a Forest, node_id: NodeId) -> Self {
        FieldIter {
            forest,
            implied_iter: None,
            implications: forest[node_id].implications().iter(),
            field_ids: forest[node_id].field_ids().iter(),
        }
    }
}

impl<'a> std::iter::Iterator for FieldIter<'a> {
    type Item = &'a Field;

    fn next(&mut self) -> Option<Self::Item> {
        loop {
            if let Some(implied) = &mut self.implied_iter {
                if let Some(implied_field) = implied.next() {
                    return Some(implied_field);
                }
                self.implied_iter = None;
            }
            if let Some(implication) = self.implications.next() {
                self.implied_iter = Some(Box::new(self.forest.fields(implication.id)));
            } else {
                break;
            }
        }
        self.field_ids
            .next()
            .map(|field_id| &self.forest.fields[field_id.0])
    }
}

impl Forest {
    pub fn new() -> Self {
        Forest::default()
    }

    pub fn root_names(&self) -> std::slice::Iter<'_, Ident> {
        self.root_names.iter()
    }

    pub fn add_root(&mut self, root: data::Root) -> Result<(), Error> {
        let node = Node {
            properties: Default::default(),
            implications: Default::default(),
            field_ids: self.push_fields(root.fields)?,
            derived_fields: root
                .derive
                .iter()
                .map(|field| -> Result<_, Error> {
                    Ok(DerivedField {
                        name: field.name.clone(),
                        expr: self.resolve_expr(&field.expr)?,
                    })
                })
                .collect::<Result<_, _>>()?,
            branches: Default::default(),
        };
        let node_id = self.push_node(node);
        debug!("adding root '{}' as {:?}", root.name, node_id);
        self.root_names.push(root.name.clone());
        self.roots.insert(root.name, node_id);
        Ok(())
    }

    pub fn add_branch(&mut self, branch: data::Branch) -> Result<(), Error> {
        let root_id = self.named_root_id(&branch.root)?;
        let (parent_id, _trace) = self.trace(root_id, &branch.path, &branch.root)?;
        let implications = branch
            .implies
            .into_iter()
            .map(|expr| self.push_implication(expr))
            .collect::<Result<_, _>>()?;
        // FIXME: check for duplicate branch
        let node = Node {
            properties: branch.properties,
            implications,
            field_ids: self.push_fields(branch.fields)?,
            derived_fields: branch
                .derive
                .iter()
                .map(|field| -> Result<_, Error> {
                    Ok(DerivedField {
                        name: field.name.clone(),
                        expr: self.resolve_expr(&field.expr)?,
                    })
                })
                .collect::<Result<_, _>>()?,
            branches: Default::default(),
        };
        let node_id = self.push_node(node);
        let condition = self.resolve_expr(&branch.condition)?;
        let parent = &mut self.nodes[parent_id.0];
        parent.branches.push(Branch {
            name: branch.name,
            target: node_id,
            condition,
        });
        Ok(())
    }

    // In case of error, the enum may be added partially to the forest.
    pub fn add_enum(&mut self, en: data::Enum) -> Result<(), Error> {
        let mut en = en;
        let node = Node {
            properties: Default::default(),
            implications: Vec::new(),   // TODO: add support
            derived_fields: Vec::new(), // TODO: add support
            field_ids: self.push_fields(en.discriminants)?,
            branches: Vec::new(),
        };
        let node_id = self.push_node(node);
        self.root_names.push(en.name.clone());
        self.roots.insert(en.name, node_id);
        let mut branches = Vec::new();
        for variant in en.variants.drain(0..) {
            let target = Node {
                properties: variant.properties,
                implications: Vec::new(), // TODO: add support
                field_ids: self.push_fields(variant.fields)?,
                derived_fields: Vec::new(), // TODO: add support
                branches: Vec::new(),
            };
            let target_id = self.push_node(target);
            branches.push(Branch {
                name: variant.name,
                target: target_id,
                condition: self.resolve_expr(&variant.condition)?,
            });
        }
        self.nodes[node_id.0].branches = branches;
        Ok(())
    }

    pub fn nodes(&self) -> std::slice::Iter<'_, Node> {
        self.nodes.iter()
    }

    fn follow<'a>(&'a self, node_id: NodeId, branch_ids: &'a [usize]) -> BranchIter<'a> {
        let node = &self[node_id];
        BranchIter {
            forest: self,
            node,
            branch_ids,
        }
    }

    fn trace(
        &self,
        root_id: NodeId,
        path: &Path,
        root_name: &Ident,
    ) -> Result<(NodeId, Vec<usize>), Error> {
        let mut node_id = root_id;
        let mut trace = Vec::with_capacity(path.len());
        for ident in path {
            let node = &self[node_id];
            let branch_idx = match node.find_branch(ident) {
                Some(idx) => idx,
                None => {
                    return Err(InnerError::UnknownBranch {
                        root_name: root_name.clone(),
                        root_id,
                        branch_ids: trace,
                        name: ident.clone(),
                    }
                    .into())
                }
            };
            trace.push(branch_idx);
            node_id = node.branches[branch_idx].target;
        }
        Ok((node_id, trace))
    }

    pub(crate) fn fields(&self, node_id: NodeId) -> FieldIter<'_> {
        FieldIter::new(self, node_id)
    }

    // Will panic if trace contains invalid references. This should only be
    // possible if using a trace from a different forest, as all traces that can
    // be produced should be (and stay) valid for that forest.
    pub fn resolve<'a>(&'a self, decoder: &'a Decoder, shaped: &'a Shaped) -> Record<'a> {
        let tree = decoder.tree();
        Record {
            forest: self,
            tree,
            node: &tree[shaped.root_id()],
            branch_ids: shaped.branch_ids(),
            values: shaped.field_values(),
        }
    }

    fn get_root_id(&self, ident: &Ident) -> Option<NodeId> {
        self.roots.get(ident).copied()
    }

    /// Prepare a node for decoding.
    pub fn decoder(&self, root: &Ident) -> Result<Decoder, DecoderPrepareError> {
        let root_id = self
            .get_root_id(root)
            .ok_or(DecoderPrepareError::UnknownRoot)?;
        let mut prepare = decode::PrepareContext::new();
        let exec_root =
            self.prepare_node(root_id, root_id, &mut prepare, &mut decode::FieldEnv::new())?;
        Ok(prepare.finish(exec_root))
    }

    /// Prepare a node for encoding.
    pub fn encoder(&self, path: &Path) -> Result<Encoder, EncoderPrepareError> {
        let mut path_iter = path.into_iter();
        let root_name = path_iter.next().ok_or(EncoderPrepareError::EmptyPath)?;
        let root_id = self
            .get_root_id(root_name)
            .ok_or(EncoderPrepareError::UnknownPath)?;
        let root = &self.nodes[root_id.0];
        let mut current = root;
        let mut fields = Vec::new();
        let mut ctx = encode::ResolveContext::new();
        let mut env = encode::ResolveEnv::new(self, &mut ctx);
        self.push_encoder_fields(current, &mut fields, &mut env);
        for branch_name in path_iter {
            let branch_idx = current
                .find_branch(branch_name)
                .ok_or(EncoderPrepareError::UnknownPath)?;
            current = &self.nodes[current.branches[branch_idx].target.0];
            self.push_encoder_fields(current, &mut fields, &mut env);
        }
        Ok(Encoder::new(fields, ctx.nodes))
    }

    pub(crate) fn push_encoder_fields(
        &self,
        node: &Node,
        fields: &mut Vec<encode::Field>,
        env: &mut encode::ResolveEnv<'_>,
    ) {
        // TODO: implications
        for field_id in &node.field_ids {
            let field = &self.fields[field_id.0];
            fields.push(encode::Field::new(
                field.name.clone(),
                env.resolve_type_expr(&field.ty).expect("infallible"),
            ));
        }
    }

    pub(crate) fn prepare_node(
        &self,
        root_id: NodeId,
        node_id: NodeId,
        ctx: &mut decode::PrepareContext,
        field_env: &mut decode::FieldEnv,
    ) -> Result<decode::ExecNodeId, decode::PrepareError> {
        debug!(
            "preparing node {:?} (root {:?}) using {:?}",
            node_id, root_id, field_env
        );
        let node = &self.nodes[node_id.0];

        // Resolve all the implied fields
        let implications: Vec<decode::NodeApp> = node
            .implications
            .iter()
            .map(|app| -> Result<_, decode::PrepareError> {
                debug!("resolving implied fields of {:?} for {:?}", app.id, node_id);
                let mut env = decode::DecodeSynEnv {
                    fields: field_env,
                    prepared: ctx,
                    forest: self,
                };
                Ok(decode::NodeApp {
                    id: env.resolve_node(&app.id)?,
                    args: app
                        .args
                        .iter()
                        .map(|arg| env.resolve_expr(arg))
                        .collect::<Result<_, decode::PrepareError>>()?,
                })
            })
            .collect::<Result<_, _>>()?;
        for app in &implications {
            let exec_node = ctx.get(app.id).expect("unknown node application");
            field_env.extend(exec_node.field_env().clone());
        }
        // Resolve all field types, which have access to field names, but not to field types.
        field_env.push_names(
            node.field_ids
                .iter()
                .map(|field_id| &self.fields[field_id.0].name),
        );
        let fields: Vec<_> = node
            .field_ids
            .iter()
            .map(|field_id| -> Result<_, decode::PrepareError> {
                let field = &self.fields[field_id.0];
                if let TypeExpr::App(app) = &field.ty {
                    ctx.include(app.id, |ctx| {
                        self.prepare_node(app.id, app.id, ctx, &mut decode::FieldEnv::new())
                    })?;
                }
                let mut env = decode::DecodeSynEnv {
                    fields: field_env,
                    prepared: ctx,
                    forest: self,
                };
                let te = env.resolve_type_expr(&field.ty)?;
                Ok(decode::Field::new(te, field.constant.clone()))
            })
            .collect::<Result<_, _>>()?;

        field_env.resolve_types(fields.iter().map(|f| f.ty().clone()));

        // The derived fields expressions have access to the types of the actual
        // fields, but they do not have preceding derived fields in scope.
        let derived_fields = node
            .derived_fields
            .iter()
            .map(|derived| {
                trace!(
                    "derived field '{}' in {:?}: resolving expression {:?} in {:?}",
                    derived.name,
                    node_id,
                    derived.expr,
                    field_env
                );
                let mut env = decode::DecodeSynEnv {
                    fields: field_env,
                    prepared: ctx,
                    forest: self,
                };
                let resolved_expr = env.resolve_expr(&derived.expr);
                match &resolved_expr {
                    Ok(resolved) => trace!(
                        "derived field '{}' in {:?}: resolved expression {:?}",
                        derived.name,
                        node_id,
                        resolved
                    ),
                    Err(e) => trace!(
                        "derived field '{}' in {:?}: error resolving expression {:?}: {}",
                        derived.name,
                        node_id,
                        derived.expr,
                        e
                    ),
                }
                // FIXME: Calculate the actual type, or introduce a "dynamic" type.
                field_env.push(
                    derived.name.clone(),
                    decode::TypeExpr::U(decode::Expr::constant(64), decode::Expr::symbol("ne")),
                );
                resolved_expr
            })
            .collect::<Result<_, _>>()?;

        let is_root = root_id == node_id;
        let exec_id = ctx.push_node(
            ExecNode::new(
                node_id,
                node.properties.clone(),
                implications,
                fields,
                derived_fields,
                field_env.clone(),
            ),
            is_root,
        );

        // All branch conditions and target nodes have access to the node's
        // parsed fields.
        let branches: Vec<_> = node
            .branches
            .iter()
            .map(|branch| -> Result<_, decode::PrepareError> {
                debug!(
                    "preparing branch {} (target {:?}) of {:?}",
                    branch.name, branch.target, node_id
                );
                let mut env = decode::DecodeSynEnv {
                    fields: field_env,
                    prepared: ctx,
                    forest: self,
                };
                let cond = env.resolve_expr(&branch.condition)?;
                let target_id =
                    self.prepare_node(root_id, branch.target, ctx, &mut field_env.clone())?;
                Ok(decode::Branch::new(cond, target_id))
            })
            .collect::<Result<_, _>>()?;
        ctx.set_branches(exec_id, branches);
        debug!("node {:?} sucessfully prepared as {:?}", node_id, exec_id);
        Ok(exec_id)
    }

    fn push_node(&mut self, node: Node) -> NodeId {
        let id = self.nodes.len();
        self.nodes.push(node);
        NodeId(id)
    }

    fn named_root_id(&self, name: &Ident) -> Result<NodeId, Error> {
        self.roots
            .get(name)
            .copied()
            .ok_or_else(|| InnerError::UnknownRoot(name.clone()).into())
    }

    fn push_implication(&mut self, expr: syn::Expr) -> Result<NodeApp, Error> {
        match expr {
            syn::Expr::Ident(name) => Ok(NodeApp {
                id: self.named_root_id(&name)?,
                args: Default::default(),
            }),
            syn::Expr::Apply(name_expr, args, named_args) => {
                if !named_args.is_empty() {
                    unimplemented!()
                }
                match *name_expr {
                    syn::Expr::Ident(name) => Ok(NodeApp {
                        id: self.named_root_id(&name)?,
                        args: args
                            .iter()
                            .map(|arg| self.resolve_expr(arg))
                            .collect::<Result<_, _>>()?,
                    }),
                    _ => unimplemented!(),
                }
            }
            _ => unimplemented!(),
        }
    }

    fn resolve_type_expr(&self, ty_expr: &data::Expr) -> Result<TypeExpr, Error> {
        use data::Expr::*;
        match ty_expr {
            Ident(ident) => {
                let name = ident.as_str();
                match name.as_bytes()[0] {
                    b'i' => {
                        let n: u32 = name[1..]
                            .parse()
                            .map_err(|_| InnerError::InvalidTypeName(ident.clone()))?;
                        Ok(TypeExpr::I(Expr::constant(n), Expr::symbol("be")))
                    }
                    b'u' => {
                        let n: u32 = name[1..]
                            .parse()
                            .map_err(|_| InnerError::InvalidTypeName(ident.clone()))?;
                        Ok(TypeExpr::U(Expr::constant(n), Expr::symbol("be")))
                    }
                    _ => {
                        let node_id = self.named_root_id(ident)?;
                        Ok(TypeExpr::ident(node_id))
                    }
                }
            }
            Apply(op, args, named_args) => {
                let ident = op.as_ident().ok_or(InnerError::ExpectedTypeConstructor)?;
                if !named_args.is_empty() {
                    return Err(InnerError::BadTypeApplication {
                        constructor: ident.clone(),
                        error: TypeConstructError::KeywordArgumentsUnsupported,
                    }
                    .into());
                }
                match ident.as_str() {
                    "i" => {
                        let [width, endianness] = self.resolve_int_type_args(ident, args)?;
                        Ok(TypeExpr::I(width, endianness))
                    }
                    "u" => {
                        let [width, endianness] = self.resolve_int_type_args(ident, args)?;
                        Ok(TypeExpr::U(width, endianness))
                    }
                    "array" => self.resolve_array_type_expr(ident, args),
                    "delimited-array" => self.resolve_delimited_array_type_expr(ident, args),
                    _ => {
                        let name = ident.as_str();
                        if name.len() >= 2 {
                            match name.as_bytes()[0] {
                                b'i' => {
                                    if let Ok(n) = name[1..].parse() {
                                        return self.resolve_fixed_int_type_expr(
                                            ident,
                                            TypeExpr::I,
                                            n,
                                            args,
                                        );
                                    }
                                }
                                b'u' => {
                                    if let Ok(n) = name[1..].parse() {
                                        return self.resolve_fixed_int_type_expr(
                                            ident,
                                            TypeExpr::U,
                                            n,
                                            args,
                                        );
                                    }
                                }
                                _ => {}
                            }
                        }
                        let args: Vec<_> = args
                            .iter()
                            .map(|arg| self.resolve_expr(arg))
                            .collect::<Result<_, _>>()?;
                        let node_id = self.named_root_id(ident)?;
                        Ok(TypeExpr::app(node_id, args))
                    }
                }
            }
            ParamRef(_idx) => Err(InnerError::ParameterUsedAsType.into()),
            Const(_value) => Err(InnerError::ConstantUsedAsType.into()),
        }
    }

    fn resolve_array_type_expr(
        &self,
        ident: &Ident,
        args: &[syn::Expr],
    ) -> Result<TypeExpr, Error> {
        match args {
            [n, et] => Ok(TypeExpr::ArrayN(
                self.resolve_expr(n)?,
                Box::new(self.resolve_type_expr(et)?),
            )),
            [et] => Ok(TypeExpr::ArrayV(Box::new(self.resolve_type_expr(et)?))),
            _ => Err(Error::type_app_argument_count_mismatch(
                ident,
                1..=2,
                args.len(),
            )),
        }
    }

    fn resolve_delimited_array_type_expr(
        &self,
        ident: &Ident,
        args: &[syn::Expr],
    ) -> Result<TypeExpr, Error> {
        match args {
            [sentinel, et] => Ok(TypeExpr::ArrayS(
                self.resolve_expr(sentinel)?,
                Box::new(self.resolve_type_expr(et)?),
            )),
            _ => Err(Error::type_app_argument_count_mismatch(
                ident,
                2..=2,
                args.len(),
            )),
        }
    }

    fn resolve_int_type_args(&self, ident: &Ident, args: &[syn::Expr]) -> Result<[Expr; 2], Error> {
        match args {
            [width, endianness] => Ok([self.resolve_expr(width)?, self.resolve_expr(endianness)?]),
            [width] => Ok([self.resolve_expr(width)?, Expr::symbol("be")]),
            _ => Err(Error::type_app_argument_count_mismatch(
                ident,
                1..=2,
                args.len(),
            )),
        }
    }

    fn resolve_fixed_int_type_expr<F>(
        &self,
        ident: &Ident,
        f: F,
        width: u8,
        args: &[syn::Expr],
    ) -> Result<TypeExpr, Error>
    where
        F: FnOnce(Expr, Expr) -> TypeExpr,
    {
        let endianness = match args {
            [] => Expr::symbol("be"),
            [endianness] => self.resolve_expr(endianness)?,
            _ => {
                return Err(Error::type_app_argument_count_mismatch(
                    ident,
                    0..=1,
                    args.len(),
                ))
            }
        };
        Ok(f(Expr::Const(width.into()), endianness))
    }

    fn push_fields(&mut self, fields: Vec<data::Field>) -> Result<Vec<FieldId>, Error> {
        let mut fields = fields;
        let field_ids = fields
            .drain(0..)
            .map(|field| -> Result<_, Error> {
                let id = self.fields.len();
                let ty = self.resolve_type_expr(&field.ty)?;
                self.fields.push(Field {
                    name: field.name,
                    ty,
                    constant: field.constant,
                    hidden: field.hidden,
                });
                Ok(FieldId(id))
            })
            .collect::<Result<_, _>>()?;
        Ok(field_ids)
    }

    fn resolve_node(&self, node_name: &Ident) -> Result<NodeId, Error> {
        self.named_root_id(node_name)
    }

    fn resolve_expr(&self, expr: &syn::Expr) -> Result<Expr, Error> {
        let resolve_args = |args: &[syn::Expr]| {
            args.iter()
                .map(|expr| self.resolve_expr(expr))
                .collect::<Result<_, _>>()
        };
        let resolve_named_args = |named_args: &HashMap<String, syn::Expr>| {
            named_args
                .iter()
                .map(|(name, expr)| -> Result<_, Error> {
                    Ok((name.clone(), self.resolve_expr(expr)?))
                })
                .collect::<Result<_, _>>()
        };
        match expr {
            syn::Expr::Ident(ident) => Ok(Expr::FieldRef(ident.clone())),
            syn::Expr::ParamRef(idx) => Ok(Expr::ParamRef(*idx)),
            syn::Expr::Apply(op, args, named_args) => match &**op {
                syn::Expr::ParamRef(_) => Err(InnerError::ParamInOperatorPosition.into()),
                syn::Expr::Const(_) => Err(InnerError::ConstantInOperatorPosition.into()),
                syn::Expr::Ident(name) => {
                    let primitive = match name.as_str() {
                        "type-property" => Primitive::TypeProperty,
                        "not" => Primitive::Not,
                        "and" => Primitive::And,
                        "or" => Primitive::Or,
                        "=" => Primitive::Eq,
                        "+" => Primitive::Add,
                        "-" => Primitive::Sub,
                        "*" => Primitive::Mul,
                        "bitwise-not" => Primitive::BitNot,
                        "bitwise-and" => Primitive::BitAnd,
                        "bitwise-or" => Primitive::BitOr,
                        "bitwise-shift-left" => Primitive::BitShl,
                        "bitwise-shift-right" => Primitive::BitShr,
                        "pad" => Primitive::Pad,
                        _ => {
                            return Ok(Expr::Construct(
                                Box::new(TypeExpr::ident(self.resolve_node(name)?)),
                                resolve_args(args)?,
                                resolve_named_args(named_args)?,
                            ))
                        }
                    };
                    if !named_args.is_empty() {
                        return Err(InnerError::KeywordArgumentForPrimitive.into());
                    }
                    Ok(Expr::Apply(primitive, resolve_args(args)?))
                }
                inner @ syn::Expr::Apply(..) => {
                    let type_expr = self.resolve_type_expr(inner)?;
                    Ok(Expr::Construct(
                        Box::new(type_expr),
                        resolve_args(args)?,
                        resolve_named_args(named_args)?,
                    ))
                }
            },
            syn::Expr::Const(c) => Ok(Expr::Const(c.clone())),
        }
    }
}

impl std::ops::Index<NodeId> for Forest {
    type Output = Node;

    fn index(&self, node_id: NodeId) -> &Self::Output {
        &self.nodes[node_id.0]
    }
}

#[derive(Debug)]
struct BranchIter<'a> {
    forest: &'a Forest,
    node: &'a Node,
    branch_ids: &'a [usize],
}

impl<'a> BranchIter<'a> {
    pub fn is_empty(&self) -> bool {
        self.branch_ids.is_empty()
    }
}

impl<'a> Iterator for BranchIter<'a> {
    type Item = &'a Branch;

    fn next(&mut self) -> Option<Self::Item> {
        if let Some((&branch_id, branch_ids)) = self.branch_ids.split_first() {
            let branch = &self.node.branches[branch_id];
            self.node = &self.forest[branch.target];
            self.branch_ids = branch_ids;
            Some(branch)
        } else {
            None
        }
    }
}
