use std::{
    collections::HashMap,
    convert::TryInto,
    fmt,
    ops::{BitAnd, BitOr, Shl, Shr},
};

use thiserror::Error;

use crate::{
    decode::CowValue,
    type_registry::{TypeId, TypeRegistry},
    Endianness, Expr, NodeApp, Primitive, Type, TypeExpr, Value,
};

pub trait Environment {
    type NodeId: fmt::Debug + Clone + std::hash::Hash + PartialEq + Eq;
    type FieldId: fmt::Debug + Clone;
    type Shaped: fmt::Debug + fmt::Display + Clone + PartialEq;

    fn lookup_field(&self, f: &Self::FieldId) -> Result<CowValue<'_, Self::Shaped>>;
    fn lookup_param(&self, idx: usize) -> Result<CowValue<'_, Self::Shaped>>;
    fn lookup_property(&self, r: &Self::FieldId, name: &str) -> Result<CowValue<'_, Self::Shaped>>;
    fn make_shaped(
        &self,
        type_id: TypeId,
        fields: HashMap<&str, CowValue<'_, Self::Shaped>>,
        types: &TypeRegistry<Self::NodeId, Self::Shaped>,
    ) -> Self::Shaped;
    fn cmp_eq(&self, v1: &CowValue<'_, Self::Shaped>, v2: &CowValue<'_, Self::Shaped>) -> bool;
}

// TODO: include source span information
#[derive(Error, Debug, Clone)]
pub enum Error {
    #[error("expected {expected}, got {got}")]
    ExpectedType { expected: &'static str, got: String },
    #[error("expected field reference")]
    ExpectedFieldRef,
    #[error("unsupported int width {0}")]
    UnsupportedIntWidth(u64),
    #[error("type is missing property '{0}'")]
    MissingTypeProperty(String),
    #[error("cannot coerce value")]
    CannotCoerce,
    #[error("result of applying `{0}` to {1} and {2} out range")]
    ResultOutOfRange(Primitive, u64, u64),
    #[error("wrong number of arguments to {op}: {expected} expected, {given} given")]
    WrongNumberOfArguments {
        op: Primitive,
        expected: usize,
        given: usize,
    },
    #[error("positional arguments not allowed for custom type")]
    PositionalArgumentNotAllowed,
}

impl Error {
    fn expected_field_ref() -> Self {
        Error::ExpectedFieldRef
    }
    fn expected_u64<E: fmt::Display>(arg: &E) -> Self {
        Error::ExpectedType {
            expected: "unsigned integer",
            got: arg.to_string(),
        }
    }
    fn expected_symbol<E: fmt::Display>(arg: &E) -> Self {
        Error::ExpectedType {
            expected: "symbol",
            got: arg.to_string(),
        }
    }
    fn expected_array_len<E: fmt::Display>(arg: &E) -> Self {
        Error::ExpectedType {
            expected: "array length",
            got: arg.to_string(),
        }
    }
    fn unsupported_int_width(width: u64) -> Self {
        Error::UnsupportedIntWidth(width)
    }
    fn expected_endianness<E: fmt::Display>(arg: &E) -> Self {
        Error::ExpectedType {
            expected: "endianness",
            got: arg.to_string(),
        }
    }
    fn result_out_of_range(op: Primitive, n1: u64, n2: u64) -> Self {
        Error::ResultOutOfRange(op, n1, n2)
    }
    fn wrong_number_of_arguments(op: Primitive, expected: usize, given: usize) -> Self {
        Error::WrongNumberOfArguments {
            op,
            expected,
            given,
        }
    }
}

pub type Result<T> = std::result::Result<T, Error>;
type EnvExpr<E> = Expr<<E as Environment>::FieldId, <E as Environment>::NodeId>;
type EnvTypeExpr<E> = TypeExpr<<E as Environment>::NodeId, EnvExpr<E>>;

pub fn eval_type_expr<E>(
    te: &EnvTypeExpr<E>,
    env: &E,
    types: &mut TypeRegistry<E::NodeId, E::Shaped>,
) -> Result<Type<TypeId, E::Shaped>>
where
    E: Environment,
{
    use crate::TypeExpr::*;
    match te {
        U(width, endianness) => {
            let (width, endianness) = eval_int_type_args(width, endianness, env, types)?;
            Ok(Type::U(width, endianness))
        }
        I(width, endianness) => {
            let (width, endianness) = eval_int_type_args(width, endianness, env, types)?;
            Ok(Type::I(width, endianness))
        }
        ArrayN(n, et) => {
            let n = eval_expr(n, env, types)?;
            let n = n
                .as_u64()
                .ok_or_else(|| Error::expected_array_len(&n))?
                .try_into()
                .map_err(|_| Error::expected_array_len(&n))?;
            let et = eval_type_expr(et, env, types)?;
            Ok(Type::ArrayN(n, Box::new(et)))
        }
        ArrayV(et) => {
            let et = eval_type_expr(et, env, types)?;
            Ok(Type::ArrayV(Box::new(et)))
        }
        ArrayS(sentinel, et) => {
            let et = eval_type_expr(et, env, types)?;
            let sentinel = eval_expr(sentinel, env, types)?
                .convert_to(&et, env, types)
                .ok_or(Error::CannotCoerce)?;
            Ok(Type::ArrayS(sentinel, Box::new(et)))
        }
        App(app) => Ok(Type::Ident(eval_app(app, env, types)?)),
    }
}

type EnvNodeApp<E> = NodeApp<
    <E as Environment>::NodeId,
    Expr<<E as Environment>::FieldId, <E as Environment>::NodeId>,
>;

pub fn eval_app<E: Environment>(
    app: &EnvNodeApp<E>,
    env: &E,
    types: &mut TypeRegistry<E::NodeId, E::Shaped>,
) -> Result<TypeId> {
    let args: Vec<_> = app
        .args
        .iter()
        .map(|arg| eval_expr(arg, env, types).map(|arg| arg.into_owned()))
        .collect::<Result<_>>()?;
    // TODO: check args conform to `node_id` parameter list
    Ok(types.register_type(app.id.clone(), args))
}

fn eval_int_type_args<E: Environment>(
    width: &Expr<E::FieldId, E::NodeId>,
    endianness: &Expr<E::FieldId, E::NodeId>,
    env: &E,
    types: &mut TypeRegistry<E::NodeId, E::Shaped>,
) -> Result<(u8, Endianness)> {
    let width = eval_expr(width, env, types)?;
    let width = width.as_u64().ok_or_else(|| Error::expected_u64(&width))?;
    let endianness = eval_expr(endianness, env, types)?;
    let endianness = endianness
        .as_endianness()
        .ok_or_else(|| Error::expected_endianness(&endianness))?;
    if width > 128 {
        return Err(Error::unsupported_int_width(width));
    }
    Ok((width as u8, endianness))
}

pub fn eval_expr<'a, E>(
    expr: &'a Expr<E::FieldId, E::NodeId>,
    env: &'a E,
    types: &mut TypeRegistry<E::NodeId, E::Shaped>,
) -> Result<CowValue<'a, E::Shaped>>
where
    E: Environment,
{
    use crate::Expr::*;
    match expr {
        ParamRef(r) => env.lookup_param(*r),
        FieldRef(r) => env.lookup_field(r),
        Const(value) => Ok(CowValue::Syntactic(value)),
        Apply(proc, args) => eval_proc_application(*proc, args, env, types),
        Construct(ty_expr, args, named_args) => {
            let ty = eval_type_expr(ty_expr, env, types)?;
            if !args.is_empty() {
                return Err(Error::PositionalArgumentNotAllowed);
            }
            let named_args: HashMap<_, _> = named_args
                .iter()
                .map(|(name, arg)| Ok((name.as_ref(), eval_expr(arg, env, types)?)))
                .collect::<Result<_>>()?;
            match ty {
                Type::Ident(type_id) => Ok(CowValue::Owned(Value::Shaped(
                    env.make_shaped(type_id, named_args, types),
                ))),
                _ => todo!("constructing values of built-in types"),
            }
        }
    }
}

fn eval_proc_application<'a, E>(
    proc: Primitive,
    args: &'a [Expr<E::FieldId, E::NodeId>],
    env: &'a E,
    types: &mut TypeRegistry<E::NodeId, E::Shaped>,
) -> Result<CowValue<'a, E::Shaped>>
where
    E: Environment,
{
    use Primitive::*;
    match proc {
        TypeProperty => match args {
            [field, name] => {
                let field_id = field.as_field_ref().ok_or_else(Error::expected_field_ref)?;
                let name = eval_expr(name, env, types)?;
                match name.as_symbol() {
                    Some(name) => env.lookup_property(field_id, name),
                    None => Err(Error::expected_symbol(&name)),
                }
            }
            _ => Err(Error::wrong_number_of_arguments(proc, 2, args.len())),
        },
        Eq => {
            let [v1, v2] = eval_2(proc, args, env, types)?;
            Ok(CowValue::Owned(Value::Bool(env.cmp_eq(&v1, &v2))))
        }
        Not => Ok(CowValue::Owned(Value::Bool(
            !eval_1(proc, args, env, types)?.is_true(),
        ))),
        And => {
            let mut last = None;
            for arg in args {
                let value = eval_expr(arg, env, types)?;
                if !value.is_true() {
                    return Ok(value);
                }
                last = Some(value);
            }
            if let Some(last) = last {
                Ok(last)
            } else {
                Ok(CowValue::Owned(Value::Bool(true)))
            }
        }
        Or => {
            for arg in args {
                let value = eval_expr(arg, env, types)?;
                if value.is_true() {
                    return Ok(value);
                }
            }
            Ok(CowValue::Owned(Value::Bool(false)))
        }
        Add => numeric_binop(proc, u64::checked_add, args, env, types)
            .map(|n| CowValue::Owned(Value::U64(n))),
        Sub => numeric_binop(proc, u64::checked_sub, args, env, types)
            .map(|n| CowValue::Owned(Value::U64(n))),
        Mul => numeric_binop(proc, u64::checked_mul, args, env, types)
            .map(|n| CowValue::Owned(Value::U64(n))),
        BitNot => {
            let n = eval_1(proc, args, env, types)?;
            let n = n.as_u64().ok_or_else(|| Error::expected_u64(&n))?;
            Ok(CowValue::Owned(Value::U64(!n)))
        }
        BitAnd => bit_nary_op(u64::bitand, args, u64::MAX, env, types)
            .map(|n| CowValue::Owned(Value::U64(n))),
        BitOr => {
            bit_nary_op(u64::bitor, args, 0, env, types).map(|n| CowValue::Owned(Value::U64(n)))
        }
        BitShl => {
            bit_binop(proc, u64::shl, args, env, types).map(|n| CowValue::Owned(Value::U64(n)))
        }
        BitShr => {
            bit_binop(proc, u64::shr, args, env, types).map(|n| CowValue::Owned(Value::U64(n)))
        }
        Pad => {
            numeric_binop(proc, pad_u64, args, env, types).map(|n| CowValue::Owned(Value::U64(n)))
        }
    }
}

fn eval_1<'a, E>(
    op: Primitive,
    args: &'a [Expr<E::FieldId, E::NodeId>],
    env: &'a E,
    types: &mut TypeRegistry<E::NodeId, E::Shaped>,
) -> Result<CowValue<'a, E::Shaped>>
where
    E: Environment,
{
    match args {
        [e] => Ok(eval_expr(e, env, types)?),
        _ => Err(Error::wrong_number_of_arguments(op, 1, args.len())),
    }
}

fn eval_2<'a, E>(
    op: Primitive,
    args: &'a [Expr<E::FieldId, E::NodeId>],
    env: &'a E,
    types: &mut TypeRegistry<E::NodeId, E::Shaped>,
) -> Result<[CowValue<'a, E::Shaped>; 2]>
where
    E: Environment,
{
    match args {
        [e1, e2] => Ok([eval_expr(e1, env, types)?, eval_expr(e2, env, types)?]),
        _ => Err(Error::wrong_number_of_arguments(op, 2, args.len())),
    }
}

fn numeric_binop<E, F>(
    op: Primitive,
    f: F,
    args: &[Expr<E::FieldId, E::NodeId>],
    env: &E,
    types: &mut TypeRegistry<E::NodeId, E::Shaped>,
) -> Result<u64>
where
    E: Environment,
    F: FnOnce(u64, u64) -> Option<u64>,
{
    match args {
        [e1, e2] => {
            let n1 = eval_expr(e1, env, types)?;
            let n2 = eval_expr(e2, env, types)?;
            match (n1.as_u64(), n2.as_u64()) {
                (Some(n1), Some(n2)) => {
                    f(n1, n2).ok_or_else(|| Error::result_out_of_range(op, n1, n2))
                }
                (None, _) => Err(Error::expected_u64(&n1)),
                (Some(_), _) => Err(Error::expected_u64(&n2)),
            }
        }
        _ => Err(Error::wrong_number_of_arguments(op, 2, args.len())),
    }
}

fn bit_binop<E, F>(
    op: Primitive,
    f: F,
    args: &[Expr<E::FieldId, E::NodeId>],
    env: &E,
    types: &mut TypeRegistry<E::NodeId, E::Shaped>,
) -> Result<u64>
where
    E: Environment,
    F: FnOnce(u64, u64) -> u64,
{
    match args {
        [e1, e2] => {
            let n1 = eval_expr(e1, env, types)?;
            let n2 = eval_expr(e2, env, types)?;
            match (n1.as_u64(), n2.as_u64()) {
                (Some(n1), Some(n2)) => Ok(f(n1, n2)),
                (None, _) => Err(Error::expected_u64(&n1)),
                (Some(_), _) => Err(Error::expected_u64(&n2)),
            }
        }
        _ => Err(Error::wrong_number_of_arguments(op, 2, args.len())),
    }
}

fn bit_nary_op<E, F>(
    f: F,
    args: &[Expr<E::FieldId, E::NodeId>],
    seed: u64,
    env: &E,
    types: &mut TypeRegistry<E::NodeId, E::Shaped>,
) -> Result<u64>
where
    E: Environment,
    F: Fn(u64, u64) -> u64,
{
    let mut acc = seed;
    for arg in args {
        let n = eval_expr(arg, env, types)?;
        match n.as_u64() {
            Some(n) => acc = f(acc, n),
            None => return Err(Error::expected_u64(&n)),
        }
    }
    Ok(acc)
}

fn pad_u64(n1: u64, n2: u64) -> Option<u64> {
    let modulo = n1 % n2;
    if modulo == 0 {
        Some(0)
    } else {
        Some(n2 - modulo)
    }
}
