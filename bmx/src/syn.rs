use std::{collections::HashMap, fmt, io};

use lexpr::datum::{self, ListIter};

use crate::{data, DerivedField, Ident};

pub type Field = crate::Field<Expr>;

#[derive(Debug, Clone)]
pub enum Expr {
    Ident(Ident),
    ParamRef(usize),
    Apply(Box<Expr>, Vec<Expr>, HashMap<String, Expr>),
    Const(lexpr::Value),
}

impl Expr {
    pub fn ident(name: impl Into<Ident>) -> Self {
        Expr::Ident(name.into())
    }
    pub fn as_ident(&self) -> Option<&Ident> {
        match self {
            Expr::Ident(ident) => Some(ident),
            _ => None,
        }
    }
    pub fn constant(value: impl Into<lexpr::Value>) -> Self {
        Expr::Const(value.into())
    }
    pub fn symbol(name: &str) -> Self {
        Expr::Const(lexpr::Value::symbol(name))
    }
    pub fn apply(op: Expr, args: Vec<Expr>, named_args: HashMap<String, Expr>) -> Self {
        Expr::Apply(Box::new(op), args, named_args)
    }
    pub fn apply_positional(op: Expr, args: Vec<Expr>) -> Self {
        Expr::Apply(Box::new(op), args, Default::default())
    }
}

/// Syntactic environment.
#[derive(Debug, Clone)]
struct Env(Vec<(Ident, Expr)>);

impl Env {
    fn new() -> Self {
        let predefined = ["le", "be", "ne"]
            .iter()
            .map(|&name| (name.into(), Expr::symbol(name)))
            .collect();
        Env(predefined)
    }

    fn from_params<P>(parameters: P) -> Self
    where
        P: IntoIterator<Item = Ident>,
    {
        let mut env = Env::new();
        env.extend(
            parameters
                .into_iter()
                .enumerate()
                .map(|(i, ident)| (ident, Expr::ParamRef(i))),
        );
        env
    }

    fn lookup(&self, name: &str) -> Option<&Expr> {
        self.0.iter().find_map(|(ident, arg)| {
            if ident.as_str() == name {
                Some(arg)
            } else {
                None
            }
        })
    }

    fn push(&mut self, name: Ident, expr: Expr) {
        self.0.push((name, expr))
    }

    fn extend(&mut self, iter: impl IntoIterator<Item = (Ident, Expr)>) {
        self.0.extend(iter)
    }
}

#[derive(thiserror::Error, Debug)]
#[error(transparent)]
pub struct Error(InnerError);

impl Error {
    pub(crate) fn span(&self) -> Option<datum::Span> {
        use InnerError::*;
        match &self.0 {
            Parse(_) => None,
            PrematureEol(_) => None,
            Expected(_, datum) => Some(datum.span()),
            ImproperList(datum) => Some(datum.span()),
            DuplicateFieldModifier(datum) => Some(datum.span()),
            InvalidFieldModifier(datum) => Some(datum.span()),
            DuplicateProperty(datum) => Some(datum.span()),
            PositionalArgumentAfterKeywords(datum) => Some(datum.span()),
            UnknownDef(datum) => Some(datum.span()),
        }
    }
    pub(crate) fn location(&self) -> Option<lexpr::parse::error::Location> {
        use InnerError::*;
        match &self.0 {
            Parse(e) => e.location(),
            PrematureEol(_) => None, // TODO
            _ => None,
        }
    }
}

#[derive(Debug)]
struct DisplaySpan(datum::Span);

fn display_span(span: datum::Span) -> DisplaySpan {
    DisplaySpan(span)
}

impl fmt::Display for DisplaySpan {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let start = self.0.start();
        let end = self.0.end();
        write!(
            f,
            "{}:{}--{}:{}",
            start.line(),
            start.column(),
            end.line(),
            end.column()
        )
    }
}

#[derive(thiserror::Error, Debug)]
enum InnerError {
    #[error("invalid s-expression: {0}")]
    Parse(lexpr::parse::Error),
    #[error("premature end of list, expected {0}")]
    PrematureEol(&'static str),
    #[error("expected {0}, found `{}` at {}", .1.value(), display_span(.1.span()))]
    Expected(&'static str, lexpr::Datum),
    #[error("expected end of list, found improper list terminated by `{}` at {}",
            .0.value(), display_span(.0.span()))]
    ImproperList(lexpr::Datum),
    #[error("duplicate field modifier `{}` at {}", .0.value(), display_span(.0.span()))]
    DuplicateFieldModifier(lexpr::Datum),
    #[error("invalid field modifier `{}` at {}", .0.value(), display_span(.0.span()))]
    InvalidFieldModifier(lexpr::Datum),
    #[error("duplicate property `{}` at {}", .0.value(), display_span(.0.span()))]
    DuplicateProperty(lexpr::Datum),
    #[error("positional argument `{}` after keyword argument at {}", .0.value(), display_span(.0.span()))]
    PositionalArgumentAfterKeywords(lexpr::Datum),
    #[error("unknown definition `{}` at {}", .0.value(), display_span(.0.span()))]
    UnknownDef(lexpr::Datum),
}

#[allow(clippy::large_enum_variant)]
#[derive(Debug, Clone)]
pub enum Def {
    Root(data::Root),
    Branch(data::Branch),
    Enum(data::Enum),
}

impl Def {
    pub fn from_datum(datum: datum::Ref<'_>) -> Result<Def, Error> {
        if let Some(mut items) = datum.list_iter() {
            let keyword = take_datum(&mut items)?;
            let def = match keyword
                .value()
                .as_symbol()
                .ok_or_else(|| expected_error("definition keyword", keyword))?
            {
                "root" => parse_root_def(items).map(Def::Root)?,
                "branch" => parse_branch_def(items).map(Def::Branch)?,
                "enum" => parse_enum_def(items).map(Def::Enum)?,
                _ => return Err(unknown_def(keyword)),
            };
            Ok(def)
        } else {
            Err(expected_error("list", datum))
        }
    }
}

pub struct Parser<T>
where
    T: io::Read,
{
    inner: lexpr::Parser<lexpr::parse::IoRead<T>>,
}

impl<T> Parser<T>
where
    T: io::Read,
{
    pub fn new(inner: T) -> Self {
        Parser {
            inner: lexpr::Parser::from_reader(inner),
        }
    }

    pub fn parse_def(&mut self) -> Result<Option<Def>, Error> {
        let datum = match self.inner.next_datum().map_err(parse_error)? {
            Some(value) => value,
            None => return Ok(None),
        };
        Ok(Some(Def::from_datum(datum.as_ref())?))
    }
}

fn parse_root_def(mut items: ListIter<'_>) -> Result<data::Root, Error> {
    let (name, parameters) = take_type_spec(&mut items)?;
    let env = Env::from_params(parameters);
    let mut fields = None;
    let mut derive = None;
    for item in items.by_ref() {
        if let Some(mut inner) = item.list_iter() {
            let tag = take_datum(&mut inner)?;
            match tag
                .as_symbol()
                .ok_or_else(|| expected_error("root clause", tag))?
            {
                "fields" => {
                    if fields.is_some() {
                        return Err(duplicate_property(tag));
                    }
                    fields = Some(expect_field_list(inner, &env)?);
                }
                "derive" => {
                    if derive.is_some() {
                        return Err(duplicate_property(tag));
                    }
                    derive = Some(parse_derivations(inner, &env)?);
                }
                _ => return Err(unknown_property(tag)),
            }
        } else {
            return Err(expected_error("field entry", item));
        }
    }
    expect_eol_exhausted(&mut items)?;
    Ok(data::Root {
        name,
        description: None,
        fields: fields.unwrap_or_default(),
        derive: derive.unwrap_or_default(),
    })
}

fn parse_enum_def(items: ListIter<'_>) -> Result<data::Enum, Error> {
    let mut items = items;
    let (name, parameters) = take_type_spec(&mut items)?;
    let env = Env::from_params(parameters);
    let mut variants = None;
    let mut discriminants = None;
    for item in items.by_ref() {
        if let Some(mut inner) = item.list_iter() {
            let tag = take_datum(&mut inner)?;
            match tag
                .as_symbol()
                .ok_or_else(|| expected_error("enum clause keyword", tag))?
            {
                "variants" => {
                    if variants.is_some() {
                        return Err(duplicate_property(tag));
                    }
                    variants = Some(expect_variant_list(
                        inner,
                        discriminants
                            .as_ref()
                            .map(|v: &Vec<data::Field>| &v[..])
                            .unwrap_or_default(),
                        &env,
                    )?);
                }
                "discriminants" => {
                    if discriminants.is_some() {
                        return Err(duplicate_property(tag));
                    }
                    let mut fields = expect_field_list(inner, &env)?;
                    for field in &mut fields {
                        field.hidden = true;
                    }
                    discriminants = Some(fields);
                }
                _ => return Err(unknown_property(tag)),
            }
        } else {
            return Err(expected_error("enum clause", item));
        }
    }
    expect_eol_exhausted(&mut items)?;
    Ok(data::Enum {
        name,
        description: None,
        discriminants: discriminants.unwrap_or_default(),
        variants: variants.unwrap_or_default(),
    })
}

fn expect_field(field: datum::Ref<'_>, env: &Env) -> Result<data::Field, Error> {
    use lexpr::Value::*;
    let (name, mut rest) = expect_tagged_list(field)?;
    let ty = take_expr(&mut rest, env)?;
    let mut hidden = None;
    let mut constant = None;
    for modifier in rest.by_ref() {
        if let Some(mut items) = modifier.list_iter() {
            let head = take_datum(&mut items)?;
            match head
                .as_symbol()
                .ok_or_else(|| expected_error("field modifier keyword", head))?
            {
                "=" => {
                    if constant.is_some() {
                        return Err(duplicate_field_modifier(head));
                    }
                    let datum = take_datum(&mut items)?;
                    expect_eol(&mut items)?;
                    constant = Some(datum.into());
                }
                _ => return Err(invalid_field_modifier(modifier)),
            }
        } else {
            match modifier.value() {
                Symbol(name) => match name.as_ref() {
                    "hidden" => {
                        if hidden.is_some() {
                            return Err(duplicate_field_modifier(modifier));
                        }
                        hidden = Some(true);
                    }
                    _ => return Err(invalid_field_modifier(modifier)),
                },
                _ => return Err(invalid_field_modifier(modifier)),
            }
        }
    }
    expect_eol_exhausted(&mut rest)?;
    Ok(data::Field {
        name: name.into(),
        ty,
        constant,
        hidden: hidden.unwrap_or(false),
    })
}

fn expect_variant_list(
    mut items: ListIter<'_>,
    discriminants: &[data::Field],
    env: &Env,
) -> Result<Vec<data::Variant>, Error> {
    let variants = items
        .by_ref()
        .map(|item| expect_variant(item, discriminants, env))
        .collect::<Result<_, _>>()?;
    expect_eol_exhausted(&mut items)?;
    Ok(variants)
}

fn expect_list_2(datum: datum::Ref<'_>) -> Result<(datum::Ref<'_>, datum::Ref<'_>), Error> {
    if let Some(mut items) = datum.list_iter() {
        let first = take_datum(&mut items)?;
        let second = take_datum(&mut items)?;
        expect_eol(&mut items)?;
        Ok((first, second))
    } else {
        Err(expected_error("two-element list", datum))
    }
}

fn take_let_bindings(form: &mut ListIter<'_>, env: &Env) -> Result<Env, Error> {
    let bindings = take_datum(form)?;
    let mut items = bindings
        .list_iter()
        .ok_or_else(|| expected_error("let bindings", bindings))?;
    let mut let_env = env.clone();
    for item in items.by_ref() {
        let (name, expr) = expect_list_2(item)?;
        let_env.push(expect_ident(name)?.clone(), expect_expr(expr, env)?);
    }
    expect_eol_exhausted(&mut items)?;
    Ok(let_env)
}

fn expect_field_expr(expr: datum::Ref<'_>, env: &Env) -> Result<Vec<data::Field>, Error> {
    if let Some(mut items) = expr.list_iter() {
        match items.peek() {
            Some(head) if head.as_symbol() == Some("let") => {
                items.next().unwrap();
                let let_env = take_let_bindings(&mut items, env)?;
                let mut fields = Vec::new();
                for item in items.by_ref() {
                    fields.extend(expect_field_expr(item, &let_env)?);
                }
                expect_eol_exhausted(&mut items)?;
                Ok(fields)
            }
            _ => Ok(vec![expect_field(expr, env)?]),
        }
    } else {
        Ok(vec![expect_field(expr, env)?])
    }
}

fn expect_field_list(mut items: ListIter<'_>, env: &Env) -> Result<Vec<data::Field>, Error> {
    let mut fields = Vec::new();
    for item in items.by_ref() {
        fields.extend(expect_field_expr(item, env)?);
    }
    expect_eol_exhausted(&mut items)?;
    Ok(fields)
}

fn expect_variant(
    datum: datum::Ref<'_>,
    discriminants: &[data::Field],
    env: &Env,
) -> Result<data::Variant, Error> {
    let (name, mut items) = expect_tagged_list(datum)?;
    let condition = take_condition(&mut items, discriminants, env)?;
    let mut fields = None;
    let mut properties = None;
    let env = Env::new();
    for item in items.by_ref() {
        if let Some(mut inner) = item.list_iter() {
            let tag = take_datum(&mut inner)?;
            match tag
                .as_symbol()
                .ok_or_else(|| expected_error("enum clause keyword", tag))?
            {
                "fields" => {
                    if fields.is_some() {
                        return Err(duplicate_property(tag));
                    }
                    fields = Some(expect_field_list(inner, &env)?);
                }
                "properties" => {
                    if properties.is_some() {
                        return Err(duplicate_property(tag));
                    }
                    properties = Some(parse_property_list(inner)?);
                }
                _ => return Err(unknown_property(tag)),
            }
        }
    }
    expect_eol_exhausted(&mut items)?;
    Ok(data::Variant {
        name: name.into(),
        condition,
        fields: fields.unwrap_or_default(),
        properties: properties.unwrap_or_default(),
    })
}

fn parse_branch_def(items: ListIter<'_>) -> Result<data::Branch, Error> {
    let mut items = items;
    let path = take_datum(&mut items)?;
    let (root, path, name) = expect_path(path)?
        .split_branch()
        .map_err(|_| expected_error("branch path", path))?;
    let mut fields = None;
    let mut condition = None;
    let mut properties = None;
    let mut implies = None;
    let mut derive = None;
    let env = Env::new();
    for item in items.by_ref() {
        if let Some(mut inner) = item.list_iter() {
            let tag = take_datum(&mut inner)?;
            match tag
                .as_symbol()
                .ok_or_else(|| expected_error("enum clause keyword", tag))?
            {
                "fields" => {
                    if fields.is_some() {
                        return Err(duplicate_property(tag));
                    }
                    fields = Some(expect_field_list(inner, &env)?);
                }
                "condition" => {
                    if condition.is_some() {
                        return Err(duplicate_property(tag));
                    }
                    condition = Some(parse_condition(inner, &env)?);
                }
                "properties" => {
                    if properties.is_some() {
                        return Err(duplicate_property(tag));
                    }
                    properties = Some(parse_property_list(inner)?);
                }
                "implies" => {
                    if implies.is_some() {
                        return Err(duplicate_property(tag));
                    }
                    implies = Some(parse_exprs(inner, &env)?);
                }
                "derive" => {
                    if derive.is_some() {
                        return Err(duplicate_property(tag));
                    }
                    derive = Some(parse_derivations(inner, &env)?);
                }
                _ => return Err(unknown_property(tag)),
            }
        } else {
            return Err(expected_error("branch clause", item));
        }
    }
    expect_eol_exhausted(&mut items)?;
    Ok(data::Branch {
        root,
        path,
        name,
        description: None,
        implies: implies.unwrap_or_default(),
        condition: condition.unwrap_or_else(|| Expr::constant(true)),
        fields: fields.unwrap_or_default(),
        derive: derive.unwrap_or_default(),
        properties: properties.unwrap_or_default(),
    })
}

fn take_condition(
    iter: &mut ListIter<'_>,
    discriminants: &[data::Field],
    env: &Env,
) -> Result<Expr, Error> {
    let datum = iter.next().ok_or_else(|| premature_eol("value"))?;
    use lexpr::Value::*;
    match datum.value() {
        Cons(_) | Symbol(_) => Ok(expect_expr(datum, env)?),
        _ => {
            // Shorthand syntax for matching on a single discriminant against a
            // constant.
            let mut fields = discriminants.iter();
            let field_name = &fields.next().ok_or_else(|| unimplemented!())?.name;
            if fields.next().is_some() {
                unimplemented!()
            }
            Ok(Expr::apply(
                Expr::ident("="),
                vec![
                    Expr::ident(field_name.clone()),
                    Expr::constant(datum.value().clone()),
                ],
                Default::default(),
            ))
        }
    }
}

fn parse_condition(mut items: ListIter<'_>, env: &Env) -> Result<Expr, Error> {
    let clauses = items
        .by_ref()
        .map(|item| expect_expr(item, env))
        .collect::<Result<_, _>>()?;
    expect_eol_exhausted(&mut items)?;
    Ok(Expr::apply(Expr::ident("and"), clauses, Default::default()))
}

fn parse_property_list(mut items: ListIter<'_>) -> Result<HashMap<String, lexpr::Value>, Error> {
    let properties = items
        .by_ref()
        .map(expect_property)
        .collect::<Result<_, _>>()?;
    expect_eol_exhausted(&mut items)?;
    Ok(properties)
}

fn parse_derivations(mut items: ListIter<'_>, env: &Env) -> Result<Vec<DerivedField<Expr>>, Error> {
    let derivations = items
        .by_ref()
        .map(|item| expect_binding(item, env))
        .collect::<Result<_, _>>()?;
    expect_eol_exhausted(&mut items)?;
    Ok(derivations)
}

fn parse_error(e: lexpr::parse::Error) -> Error {
    Error(InnerError::Parse(e))
}

fn premature_eol(expected: &'static str) -> Error {
    Error(InnerError::PrematureEol(expected))
}

fn unknown_def(name: datum::Ref<'_>) -> Error {
    Error(InnerError::UnknownDef(name.into()))
}

fn unknown_property(property: datum::Ref<'_>) -> Error {
    expected_error("property name", property)
}

fn improper_list(rest: datum::Ref<'_>) -> Error {
    Error(InnerError::ImproperList(rest.into()))
}

fn duplicate_property(name: datum::Ref<'_>) -> Error {
    Error(InnerError::DuplicateProperty(name.into()))
}

fn duplicate_field_modifier(name: datum::Ref<'_>) -> Error {
    Error(InnerError::DuplicateFieldModifier(name.into()))
}

fn invalid_field_modifier(modifier: datum::Ref<'_>) -> Error {
    Error(InnerError::InvalidFieldModifier(modifier.into()))
}

fn expected_error(expected: &'static str, found: impl Into<lexpr::Datum>) -> Error {
    Error(InnerError::Expected(expected, found.into()))
}

fn positional_argument_after_keywords(arg: datum::Ref<'_>) -> Error {
    Error(InnerError::PositionalArgumentAfterKeywords(arg.into()))
}

fn expect_expr(datum: datum::Ref<'_>, env: &Env) -> Result<Expr, Error> {
    if let Some(mut items) = datum.list_iter() {
        if items.is_empty() {
            return Ok(Expr::Const(lexpr::Value::Null));
        }
        let op = take_expr(&mut items, env)?;
        match op {
            Expr::Ident(name) if name.as_str() == "quote" => {
                let datum = take_datum(&mut items)?;
                expect_eol(&mut items)?;
                Ok(Expr::Const(datum.value().clone()))
            }
            _ => {
                let (args, named_args) = parse_app_arguments(&mut items, env)?;
                Ok(Expr::apply(op, args, named_args))
            }
        }
    } else {
        use lexpr::Value::*;
        let expr = match datum.value() {
            b @ Bool(_) => Expr::Const(b.clone()),
            n @ Number(_) => Expr::Const(n.clone()),
            v @ Vector(_) => Expr::Const(v.clone()),
            Symbol(name) => match env.lookup(name) {
                Some(e) => e.clone(),
                None => Expr::ident(expect_ident(datum)?),
            },
            _ => unimplemented!("expression syntax {:?}", datum),
        };
        Ok(expr)
    }
}

fn expect_property(datum: datum::Ref<'_>) -> Result<(String, lexpr::Value), Error> {
    let mut items = datum
        .list_iter()
        .ok_or_else(|| expected_error("property definition", datum))?;
    let name = take_symbol(&mut items)?;
    let datum = take_datum(&mut items)?;
    expect_eol(&mut items)?;
    Ok((name.to_owned(), datum.value().clone()))
}

fn expect_tagged_list(datum: datum::Ref<'_>) -> Result<(&str, ListIter<'_>), Error> {
    let mut items = datum
        .list_iter()
        .ok_or_else(|| expected_error("tagged list", datum))?;
    let tag = take_symbol(&mut items)?;
    Ok((tag, items))
}

fn take_datum<'a>(iter: &mut ListIter<'a>) -> Result<datum::Ref<'a>, Error> {
    iter.next().ok_or_else(|| premature_eol("datum"))
}

fn take_symbol<'a>(iter: &mut ListIter<'a>) -> Result<&'a str, Error> {
    let datum = iter.next().ok_or_else(|| premature_eol("symbol"))?;
    datum
        .value()
        .as_symbol()
        .ok_or_else(|| expected_error("symbol", datum))
}

fn expect_ident(datum: datum::Ref<'_>) -> Result<Ident, Error> {
    let name = datum
        .value()
        .as_symbol()
        .ok_or_else(|| expected_error("identifier", datum))?;
    Ok(name.into())
}

fn take_ident(iter: &mut ListIter<'_>) -> Result<Ident, Error> {
    let symbol = take_symbol(iter)?;
    Ok(symbol.into())
}

fn take_type_spec(iter: &mut ListIter<'_>) -> Result<(Ident, Vec<Ident>), Error> {
    use lexpr::Value::*;
    let datum = take_datum(iter)?;
    if let Some(mut items) = datum.list_iter() {
        let name = take_ident(&mut items)?;
        let params = items.by_ref().map(expect_ident).collect::<Result<_, _>>()?;
        expect_eol_exhausted(&mut items)?;
        Ok((name, params))
    } else {
        match datum.value() {
            Symbol(name) => Ok((name.as_ref().into(), Vec::new())),
            _ => Err(expected_error("type name", datum)),
        }
    }
}

fn expect_path(datum: datum::Ref<'_>) -> Result<data::Path, Error> {
    use lexpr::Value::*;
    match datum.value() {
        Symbol(name) => Ok(name.as_ref().into()),
        _ => Err(expected_error("path", datum)),
    }
}

fn take_expr(iter: &mut ListIter<'_>, env: &Env) -> Result<Expr, Error> {
    expect_expr(take_datum(iter)?, env)
}

fn parse_exprs(mut iter: ListIter<'_>, env: &Env) -> Result<Vec<Expr>, Error> {
    let mut args = Vec::new();
    while iter.peek().is_some() {
        args.push(take_expr(&mut iter, env)?);
    }
    expect_eol_exhausted(&mut iter)?;
    Ok(args)
}

fn parse_app_arguments(
    iter: &mut ListIter<'_>,
    env: &Env,
) -> Result<(Vec<Expr>, HashMap<String, Expr>), Error> {
    let mut named_args = HashMap::new();
    let mut positional_args = Vec::new();
    loop {
        let item = match iter.next() {
            Some(item) => item,
            None => {
                expect_eol_exhausted(iter)?;
                break Ok((positional_args, named_args));
            }
        };
        match item.value() {
            lexpr::Value::Keyword(name) => {
                let expr = take_expr(iter, env)?;
                named_args.insert(name.as_ref().into(), expr); // TODO: detect duplicates
            }
            _ => {
                if !named_args.is_empty() {
                    return Err(positional_argument_after_keywords(item));
                }
                positional_args.push(expect_expr(item, env)?);
            }
        }
    }
}

fn expect_eol(iter: &mut ListIter<'_>) -> Result<(), Error> {
    match iter.next() {
        None => expect_eol_exhausted(iter),
        Some(item) => Err(expected_error("end of list", item)),
    }
}

fn expect_eol_exhausted(iter: &mut ListIter<'_>) -> Result<(), Error> {
    if iter.is_empty() {
        Ok(())
    } else {
        let rest = iter.next().unwrap();
        Err(improper_list(rest))
    }
}

fn expect_binding(datum: datum::Ref<'_>, env: &Env) -> Result<DerivedField<Expr>, Error> {
    let mut items = datum
        .list_iter()
        .ok_or_else(|| expected_error("binding", datum))?;
    let name = take_ident(&mut items)?;
    let expr = take_expr(&mut items, env)?;
    expect_eol(&mut items)?;
    Ok(DerivedField { name, expr })
}
