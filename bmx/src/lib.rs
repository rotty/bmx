#![warn(rust_2018_idioms)]

use std::{collections::HashMap, fmt, io};

mod data;
pub use data::{Ident, Path};

pub mod chex;
pub mod forest;
pub mod syn;

pub use forest::{Forest, Record};

mod decode;
pub use decode::Decoder;

mod encode;
pub use encode::Encoder;

mod value;
pub use value::Value;

mod eval;
mod resolve;
mod type_registry;

/// An expression evaluating to a type.
#[derive(Debug, Clone)]
pub enum TypeExpr<I, A> {
    U(A, A),
    I(A, A),
    ArrayN(A, Box<TypeExpr<I, A>>),
    ArrayV(Box<TypeExpr<I, A>>),
    ArrayS(A, Box<TypeExpr<I, A>>),
    App(NodeApp<I, A>),
}

impl<I, A> TypeExpr<I, A> {
    /// Construct a type expression given an identifier.
    pub fn ident(ident: I) -> Self {
        TypeExpr::App(NodeApp {
            id: ident,
            args: Vec::new(),
        })
    }

    /// Construct a type expression that is formed by the application of a
    /// parameterized type identifier.
    pub fn app(ident: I, args: impl IntoIterator<Item = A>) -> Self {
        TypeExpr::App(NodeApp {
            id: ident,
            args: args.into_iter().collect(),
        })
    }
}

/// An application of a node given a number of arguments.
#[derive(Debug, Clone)]
pub struct NodeApp<I, A> {
    pub id: I,
    pub args: Vec<A>,
}

/// The result of evaluating a `TypeExpr`.
#[derive(Debug, Clone, PartialEq)]
pub enum Type<I, S> {
    U(u8, Endianness),
    I(u8, Endianness),
    ArrayN(usize, Box<Type<I, S>>),
    ArrayV(Box<Type<I, S>>),
    ArrayS(Value<S>, Box<Type<I, S>>),
    Ident(I),
}

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub enum Endianness {
    Little,
    Big,
    Native,
}

impl Endianness {
    fn name(self) -> &'static str {
        match self {
            Endianness::Little => "le",
            Endianness::Big => "be",
            Endianness::Native => "ne",
        }
    }
}

#[derive(Debug, Clone, Copy)]
pub enum Primitive {
    Not,
    And,
    Or,
    Eq,
    Add,
    Sub,
    Mul,
    BitNot,
    BitAnd,
    BitOr,
    BitShl,
    BitShr,
    Pad,
    TypeProperty,
}

impl fmt::Display for Primitive {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        use Primitive::*;
        let name = match self {
            Not => "not",
            And => "and",
            Or => "or",
            Eq => "=",
            Add => "+",
            Sub => "-",
            Mul => "*",
            BitNot => "bitwise-not",
            BitAnd => "bitwise-and",
            BitOr => "bitwise-or",
            BitShl => "bitwise-shift-left",
            BitShr => "bitwise-shift-right",
            Pad => "pad",
            TypeProperty => "type-property",
        };
        f.write_str(name)
    }
}

#[derive(Debug, Clone)]
pub enum Expr<F, N> {
    ParamRef(usize),
    FieldRef(F),
    Apply(Primitive, Vec<Expr<F, N>>),
    Construct(
        Box<TypeExpr<N, Expr<F, N>>>,
        Vec<Expr<F, N>>,
        HashMap<String, Expr<F, N>>,
    ),
    Const(lexpr::Value),
}

impl<F, N> Expr<F, N> {
    pub fn constant(value: impl Into<lexpr::Value>) -> Self {
        Expr::Const(value.into())
    }

    pub fn symbol(name: &str) -> Self {
        Expr::Const(lexpr::Value::symbol(name))
    }

    pub fn as_field_ref(&self) -> Option<&F> {
        match self {
            Expr::FieldRef(r) => Some(r),
            _ => None,
        }
    }
    // pub fn simplified(self) -> Self {
    //     use Expr::*;
    //     match self {
    //         And(mut conds) => match conds.len() {
    //             1 => conds.pop().unwrap().simplified(),
    //             _ => And(conds.into_iter().map(Expr::simplified).collect()),
    //         },
    //         Or(mut conds) => match conds.len() {
    //             1 => conds.pop().unwrap(),
    //             _ => Or(conds.into_iter().map(Expr::simplified).collect()),
    //         },
    //         _ => self,
    //     }
    // }
    // }
}

#[derive(Debug, Clone)]
pub struct Field<T> {
    pub name: Ident,
    pub ty: T,
    pub constant: Option<lexpr::Datum>,
    pub hidden: bool,
}

#[derive(Debug, Clone)]
pub struct DerivedField<T> {
    pub name: Ident,
    pub expr: T,
}

#[derive(Debug, Clone)]
pub enum Item<'a, F, T> {
    Decoded(&'a Field<F>),
    Derived(&'a DerivedField<T>),
}

impl<'a, F, T> Item<'a, F, T> {
    pub fn name(&self) -> &'a Ident {
        match self {
            Item::Decoded(field) => &field.name,
            Item::Derived(field) => &field.name,
        }
    }
}

impl Value<Shaped> {
    fn display<'a>(&'a self, forest: &'a Forest, tree: &'a decode::Tree) -> ValueDisplay<'_> {
        ValueDisplay {
            value: self,
            forest,
            tree,
        }
    }

    fn to_sexp(&self) -> lexpr::Value {
        use Value::*;
        match self {
            Bool(b) => (*b).into(),
            U32(n) => (*n).into(),
            U64(n) => (*n).into(),
            I32(n) => (*n).into(),
            I64(n) => (*n).into(),
            Endian(e) => lexpr::Value::symbol(e.name()),
            Array(elts) => lexpr::Value::list(elts.iter().map(|elt| elt.to_sexp())),
            Shaped(shaped) => lexpr::Value::Vector(shaped.values().map(Value::to_sexp).collect()),
        }
    }

    fn to_sexp_named(&self, forest: &Forest, tree: &decode::Tree) -> lexpr::Value {
        use Value::*;
        match self {
            Bool(b) => (*b).into(),
            U32(n) => (*n).into(),
            U64(n) => (*n).into(),
            I32(n) => (*n).into(),
            I64(n) => (*n).into(),
            Endian(e) => lexpr::Value::symbol(e.name()),
            Array(elts) => {
                lexpr::Value::list(elts.iter().map(|elt| elt.to_sexp_named(forest, tree)))
            }
            Shaped(shaped) => Record::new(shaped, forest, tree).into(),
        }
    }
}

#[derive(Debug, Clone)]
pub struct Shaped {
    trace: decode::Trace,
    values: Vec<decode::FieldValue>,
}

impl PartialEq<Shaped> for Shaped {
    fn eq(&self, rhs: &Shaped) -> bool {
        if self.trace != rhs.trace {
            return false;
        }
        if self.values.len() != rhs.values.len() {
            return false;
        }
        for (v1, v2) in self.values.iter().zip(rhs.values.iter()) {
            if v1.value() != v2.value() {
                return false;
            }
        }
        true
    }
}

impl fmt::Display for Shaped {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.write_str("(")?;
        for (i, value) in self.values().enumerate() {
            if i == 0 {
                write!(f, "{}", value)?;
            } else {
                write!(f, ", {}", value)?;
            }
        }
        f.write_str(")")?;
        Ok(())
    }
}

impl Shaped {
    pub fn root_id(&self) -> decode::ExecNodeId {
        self.trace.root_id
    }

    pub fn branch_ids(&self) -> &[usize] {
        &self.trace.branch_ids
    }

    pub fn values(&self) -> impl Iterator<Item = &Value<Shaped>> {
        self.values.iter().map(|item| item.value())
    }

    pub fn field_values(&self) -> &[decode::FieldValue] {
        &self.values
    }
}

#[derive(Debug)]
pub struct ValueDisplay<'a> {
    value: &'a Value<Shaped>,
    tree: &'a crate::decode::Tree,
    forest: &'a crate::Forest,
}

impl<'a> fmt::Display for ValueDisplay<'a> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        use Value::*;
        match self.value {
            Bool(true) => write!(f, "true"),
            Bool(false) => write!(f, "false"),
            U32(n) => write!(f, "{}", n),
            U64(n) => write!(f, "{}", n),
            I32(n) => write!(f, "{}", n),
            I64(n) => write!(f, "{}", n),
            Endian(e) => write!(f, "{}", e.name()),
            Array(elts) => {
                f.write_str("[")?;
                for (i, value) in elts.iter().enumerate() {
                    if i == 0 {
                        write!(f, "{}", value.display(self.forest, self.tree))?;
                    } else {
                        write!(f, ", {}", value.display(self.forest, self.tree))?;
                    }
                }
                f.write_str("]")?;
                Ok(())
            }
            Shaped(shaped) => {
                let record = Record::new(shaped, self.forest, self.tree);
                write!(f, "{}", record)
            }
        }
    }
}

impl<I, A> fmt::Display for TypeExpr<I, A>
where
    I: fmt::Display,
    A: fmt::Display,
{
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        use TypeExpr::*;
        match self {
            U(n, e) => write!(f, "(i {} {})", n, e),
            I(n, e) => write!(f, "(u {} {})", n, e),
            ArrayN(n, et) => write!(f, "[{}; {}]", et, n),
            ArrayV(et) => write!(f, "[{}]", et),
            ArrayS(sentinel, et) => write!(f, "[{}; {}]", et, sentinel),
            App(app) => {
                write!(f, "#<Type {}", app.id)?;
                for (i, arg) in app.args.iter().enumerate() {
                    if i + 1 < app.args.len() {
                        write!(f, "{} ", arg)?;
                    } else {
                        write!(f, "{}", arg)?;
                    }
                }
                f.write_str(">")?;
                Ok(())
            }
        }
    }
}

pub fn parse_into_forest(
    forest: &mut Forest,
    datum: lexpr::datum::Ref<'_>,
) -> Result<(), ReadError> {
    use syn::Def;
    match Def::from_datum(datum)? {
        Def::Root(root) => {
            forest.add_root(root)?;
        }
        Def::Branch(branch) => forest.add_branch(branch)?,
        Def::Enum(en) => {
            forest.add_enum(en)?;
        }
    }
    Ok(())
}

pub fn read_into_forest<R: io::Read>(forest: &mut Forest, reader: &mut R) -> Result<(), ReadError> {
    use syn::Def;
    let mut parser = syn::Parser::new(reader);
    while let Some(def) = parser.parse_def()? {
        match def {
            Def::Root(root) => {
                forest.add_root(root)?;
            }
            Def::Branch(branch) => forest.add_branch(branch)?,
            Def::Enum(en) => {
                forest.add_enum(en)?;
            }
        }
    }
    Ok(())
}

#[derive(Debug)]
pub enum ReadError {
    Syntax(syn::Error),
    Forest(forest::Error),
}

#[derive(Debug)]
pub struct DisplayReadError<'a> {
    error: &'a ReadError,
    forest: &'a Forest,
}

impl<'a> fmt::Display for DisplayReadError<'a> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self.error {
            ReadError::Syntax(_) => write!(f, "{}", self.error),
            ReadError::Forest(e) => write!(f, "{}", e.display(self.forest)),
        }
    }
}

impl<'a> std::error::Error for DisplayReadError<'a> {}

impl ReadError {
    pub fn display<'a>(&'a self, forest: &'a Forest) -> DisplayReadError<'a> {
        DisplayReadError {
            error: self,
            forest,
        }
    }
    pub fn diagnostic<'a>(&'a self, forest: &'a Forest, source: &'a str) -> ReadDiagnostic<'a> {
        ReadDiagnostic {
            error: self,
            forest,
            source,
        }
    }
}

impl From<syn::Error> for ReadError {
    fn from(e: syn::Error) -> Self {
        ReadError::Syntax(e)
    }
}

impl From<forest::Error> for ReadError {
    fn from(e: forest::Error) -> Self {
        ReadError::Forest(e)
    }
}

impl fmt::Display for ReadError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            ReadError::Syntax(e) => write!(f, "syntax error: {}", e),
            ReadError::Forest(e) => write!(f, "could not add definition: {}", e),
        }
    }
}

impl std::error::Error for ReadError {}

#[derive(Debug, Clone)]
pub struct ReadDiagnostic<'a> {
    error: &'a ReadError,
    forest: &'a Forest,
    source: &'a str,
}

impl<'a> fmt::Display for ReadDiagnostic<'a> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.error.display(self.forest))
    }
}

impl<'a> std::error::Error for ReadDiagnostic<'a> {}

impl<'a> miette::Diagnostic for ReadDiagnostic<'a> {
    fn source_code(&self) -> Option<&dyn miette::SourceCode> {
        Some(&self.source)
    }

    fn labels(&self) -> Option<Box<dyn Iterator<Item = miette::LabeledSpan> + '_>> {
        self.span().map(|span| {
            Box::new(std::iter::once(span)) as Box<dyn Iterator<Item = miette::LabeledSpan>>
        })
    }
}

impl<'a> ReadDiagnostic<'a> {
    pub fn named(&self, name: impl AsRef<str>) -> NamedReadDiagnostic {
        NamedReadDiagnostic {
            message: self.error.display(self.forest).to_string(),
            source: miette::NamedSource::new(name, self.source.to_owned()),
            span: self.span(),
        }
    }

    fn span(&self) -> Option<miette::LabeledSpan> {
        match self.error {
            ReadError::Syntax(e) => match e.span() {
                Some(span) => {
                    let start = span.start();
                    let start = miette::SourceOffset::from_location(
                        self.source,
                        start.line(),
                        start.column(),
                    );
                    let end = span.end();
                    let end =
                        miette::SourceOffset::from_location(self.source, end.line(), end.column());
                    Some(miette::LabeledSpan::new_with_span(
                        None,
                        start.offset()..end.offset(),
                    ))
                }
                None => e.location().map(|pos| {
                    let pos =
                        miette::SourceOffset::from_location(self.source, pos.line(), pos.column());
                    miette::LabeledSpan::new_with_span(None, pos)
                }),
            },
            _ => None, // TODO
        }
    }
}

#[derive(Debug)]
pub struct NamedReadDiagnostic {
    message: String,
    source: miette::NamedSource,
    span: Option<miette::LabeledSpan>,
}

impl fmt::Display for NamedReadDiagnostic {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.write_str(&self.message)
    }
}

impl std::error::Error for NamedReadDiagnostic {}

impl miette::Diagnostic for NamedReadDiagnostic {
    fn source_code(&self) -> Option<&dyn miette::SourceCode> {
        Some(&self.source)
    }

    fn labels(&self) -> Option<Box<dyn Iterator<Item = miette::LabeledSpan> + '_>> {
        self.span.as_ref().map(|span| {
            Box::new(std::iter::once(span.clone())) as Box<dyn Iterator<Item = miette::LabeledSpan>>
        })
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::collections::HashMap;

    fn single_field_record(ty: data::Expr) -> data::Root {
        data::Root {
            name: "Foo".into(),
            description: None,
            fields: vec![data::Field {
                name: "bar".into(),
                ty,
                constant: None,
                hidden: false,
            }],
            derive: vec![],
        }
    }

    fn u_type(width: u8, endianness: &str) -> data::Expr {
        data::Expr::apply_positional(
            data::Expr::ident("u"),
            vec![data::Expr::constant(width), data::Expr::symbol(endianness)],
        )
    }

    fn single_u_field_record(width: u8, endianness: &str) -> data::Root {
        single_field_record(u_type(width, endianness))
    }

    fn check_u_field(width: u8, endianness: &str, value: u64, expected: &[u8]) {
        let mut forest = Forest::new();
        forest
            .add_root(single_u_field_record(width, endianness))
            .unwrap();
        let encoder = forest
            .encoder(&"Foo".into())
            .expect("cannot construct encoder");
        let fields: HashMap<_, _> = vec![(Ident::from("bar"), lexpr::Value::from(value))]
            .into_iter()
            .collect();
        assert_eq!(encoder.encode(&fields).unwrap(), expected);
    }

    #[test]
    fn test_encode_u_field() {
        check_u_field(8, "be", 42, &[42]);
        check_u_field(16, "be", 0x1234, &[0x12, 0x34]);
        check_u_field(16, "le", 0x1234, &[0x34, 0x12]);
        check_u_field(32, "be", 0x12345678, &[0x12, 0x34, 0x56, 0x78]);
        check_u_field(
            64,
            "be",
            0x123456789ABCDEF,
            &[0x01, 0x23, 0x45, 0x67, 0x89, 0xAB, 0xCD, 0xEF],
        );
        check_u_field(
            64,
            "le",
            0x123456789ABCDEF,
            &[0xEF, 0xCD, 0xAB, 0x89, 0x67, 0x45, 0x23, 0x01],
        );
        check_u_field(
            128,
            "be",
            0x123456789ABCDEF,
            &[
                0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x23, 0x45, 0x67, 0x89, 0xAB,
                0xCD, 0xEF,
            ],
        );
        check_u_field(
            128,
            "le",
            0x123456789ABCDEF,
            &[
                0xEF, 0xCD, 0xAB, 0x89, 0x67, 0x45, 0x23, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                0x00, 0x00,
            ],
        );
    }

    #[test]
    fn test_encode_bitfield() {
        let root = data::Root {
            name: "Foo".into(),
            description: None,
            fields: vec![
                data::Field {
                    name: "bar".into(),
                    ty: u_type(1, "be"),
                    constant: None,
                    hidden: false,
                },
                data::Field {
                    name: "baz".into(),
                    ty: u_type(3, "be"),
                    constant: None,
                    hidden: false,
                },
                data::Field {
                    name: "qux".into(),
                    ty: u_type(4, "be"),
                    constant: None,
                    hidden: false,
                },
                data::Field {
                    name: "frob".into(),
                    ty: u_type(8, "be"),
                    constant: None,
                    hidden: false,
                },
            ],
            derive: vec![],
        };
        let mut forest = Forest::new();
        forest.add_root(root).unwrap();
        let encoder = forest
            .encoder(&"Foo".into())
            .expect("cannot construct encoder");
        let fields: HashMap<_, _> = vec![
            ("bar", 1),
            ("baz", 0b101),
            ("qux", 0b1011),
            ("frob", 0b01100101),
        ]
        .into_iter()
        .map(|(name, value)| (Ident::from(name), lexpr::Value::from(value)))
        .collect();
        assert_eq!(encoder.encode(&fields).unwrap(), &[0b11011011, 0b01100101]);
    }
}
