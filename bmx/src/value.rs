use std::fmt;

use crate::Endianness;

#[derive(Debug, Clone)]
pub enum Value<S> {
    Bool(bool),
    U32(u32),
    U64(u64),
    I32(i32),
    I64(i64),
    Endian(Endianness),
    // TODO: this has the invariant of having all elements of the same type,
    // which should be encoded into the Rust type.
    Array(Vec<Value<S>>),
    Shaped(S),
}

impl<S> Value<S> {
    pub fn as_u64(&self) -> Option<u64> {
        use Value::*;
        match self {
            U32(n) => Some((*n).into()),
            U64(n) => Some(*n),
            I32(n) => (*n).try_into().ok(),
            I64(n) => (*n).try_into().ok(),
            _ => None,
        }
    }

    pub fn as_i64(&self) -> Option<i64> {
        use Value::*;
        match self {
            U32(n) => (*n).try_into().ok(),
            U64(n) => (*n).try_into().ok(),
            I32(n) => Some((*n).into()),
            I64(n) => Some(*n),
            _ => None,
        }
    }

    pub fn is_true(&self) -> bool {
        !matches!(self, Value::Bool(false))
    }
}

impl<S> AsRef<Value<S>> for Value<S> {
    fn as_ref(&self) -> &Value<S> {
        self
    }
}

impl<S> PartialEq for Value<S>
where
    S: PartialEq<S>,
{
    fn eq(&self, other: &Value<S>) -> bool {
        use Value::*;
        match (self, other) {
            (Bool(b1), Bool(b2)) => b1 == b2,
            (U32(n1), U32(n2)) => n1 == n2,
            (U64(n1), U64(n2)) => n1 == n2,
            (I32(n1), I32(n2)) => n1 == n2,
            (I64(n1), I64(n2)) => n1 == n2,
            (Array(a1), Array(a2)) => {
                if a1.len() == a2.len() {
                    for (v1, v2) in a1.iter().zip(a2) {
                        if v1 != v2 {
                            return false;
                        }
                    }
                    true
                } else {
                    false
                }
            }
            (Endian(e1), Endian(e2)) => e1 == e2,
            (Shaped(s1), Shaped(s2)) => s1 == s2,
            _ => false,
        }
    }
}

impl<S> PartialEq<lexpr::Value> for Value<S> {
    fn eq(&self, other: &lexpr::Value) -> bool {
        use lexpr::Value::*;
        match (self, other) {
            (Value::Bool(b1), Bool(b2)) => b1 == b2,
            (Value::U32(n1), Number(n2)) => Some(u64::from(*n1)) == n2.as_u64(),
            (Value::U64(n1), Number(n2)) => Some(*n1) == n2.as_u64(),
            (Value::I32(n1), Number(n2)) => Some(i64::from(*n1)) == n2.as_i64(),
            (Value::I64(n1), Number(n2)) => Some(*n1) == n2.as_i64(),
            (Value::Array(a1), Vector(a2)) => {
                if a1.len() == a2.len() {
                    for (v1, v2) in a1.iter().zip(a2.as_ref()) {
                        if v1 != v2 {
                            return false;
                        }
                    }
                    true
                } else {
                    false
                }
            }
            _ => unimplemented!("compound comparisons (rhs: {:?})", other),
        }
    }
}

impl<S> PartialEq<Value<S>> for lexpr::Value {
    fn eq(&self, other: &Value<S>) -> bool {
        other == self
    }
}

impl<S> From<i64> for Value<S> {
    fn from(n: i64) -> Self {
        if let Ok(n) = i32::try_from(n) {
            Value::I32(n)
        } else {
            Value::I64(n)
        }
    }
}

impl<S> fmt::Display for Value<S>
where
    S: fmt::Display,
{
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        use Value::*;
        match self {
            Bool(true) => write!(f, "true"),
            Bool(false) => write!(f, "false"),
            U32(n) => write!(f, "{}", n),
            U64(n) => write!(f, "{}", n),
            I32(n) => write!(f, "{}", n),
            I64(n) => write!(f, "{}", n),
            Endian(e) => write!(f, "{}", e.name()),
            Array(elts) => {
                f.write_str("[")?;
                for (i, value) in elts.iter().enumerate() {
                    if i == 0 {
                        write!(f, "{}", value)?;
                    } else {
                        write!(f, ", {}", value)?;
                    }
                }
                f.write_str("]")?;
                Ok(())
            }
            Shaped(shaped) => {
                write!(f, "{}", shaped)
            }
        }
    }
}
