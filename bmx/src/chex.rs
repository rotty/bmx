/// Commented-Hex input format
use std::{fmt, io};

#[derive(Debug)]
pub struct HexReader<R> {
    decoder: HexDecoder,
    reader: R,
}

impl<R> HexReader<R> {
    pub fn new(reader: R) -> Self {
        HexReader {
            reader,
            decoder: Default::default(),
        }
    }
}

#[derive(Debug, Copy, Clone)]
enum HexState {
    Comment(Nibble),
    Data(Nibble),
}

#[derive(Debug, Copy, Clone)]
enum Nibble {
    Low,
    High,
}

fn as_nibble(octet: u8) -> Option<u8> {
    match octet {
        b'0'..=b'9' => Some(octet - b'0'),
        b'a'..=b'f' => Some(10 + (octet - b'a')),
        b'A'..=b'F' => Some(10 + (octet - b'A')),
        _ => None,
    }
}

#[derive(Debug)]
struct HexDecoder {
    index: usize,
    state: HexState,
    acc: u8,
}

#[derive(Debug, Clone)]
pub enum DecodeError {
    PrematureEndOfInput,
    InvalidByte { index: usize, byte: u8 },
}

impl DecodeError {
    fn into_io(self) -> io::Error {
        use DecodeError::*;
        match self {
            PrematureEndOfInput => io::Error::new(
                io::ErrorKind::InvalidData,
                "End of input when expecting low nibble",
            ),
            InvalidByte { .. } => {
                io::Error::new(io::ErrorKind::InvalidData, "Invalid byte encountered")
            }
        }
    }
}

impl fmt::Display for DecodeError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        use DecodeError::*;
        match self {
            PrematureEndOfInput => write!(f, "End of input when expecting low nibble"),
            InvalidByte { byte, index } => {
                write!(f, "Invalid byte `b{:?}`, at index {}", *byte as char, index)
            }
        }
    }
}

impl std::error::Error for DecodeError {}

impl Default for HexDecoder {
    fn default() -> Self {
        HexDecoder::new()
    }
}

impl HexDecoder {
    fn new() -> Self {
        HexDecoder {
            state: HexState::Data(Nibble::High),
            acc: 0,
            index: 0,
        }
    }

    fn push(&mut self, octet: u8) -> Result<Option<u8>, DecodeError> {
        use HexState::*;
        use Nibble::*;

        let result = match self.state {
            Comment(n) => {
                if octet == b'\n' {
                    self.state = Data(n);
                }
                Ok(None)
            }
            Data(n) => match as_nibble(octet) {
                Some(nibble) => match n {
                    High => {
                        self.acc = nibble << 4;
                        self.state = Data(Low);
                        Ok(None)
                    }
                    Low => {
                        let octet = self.acc | nibble;
                        self.acc = 0;
                        self.state = Data(High);
                        Ok(Some(octet))
                    }
                },
                None => match octet {
                    b'#' => {
                        self.state = Comment(n);
                        Ok(None)
                    }
                    b'\n' | b' ' | b'\t' | 12 => Ok(None),
                    _ => Err(DecodeError::InvalidByte {
                        byte: octet,
                        index: self.index,
                    }),
                },
            },
        };
        self.index += 1;
        result
    }

    fn finish(&self) -> Result<(), DecodeError> {
        use HexState::*;
        use Nibble::*;
        match self.state {
            Data(High) | Comment(High) => Ok(()),
            _ => Err(DecodeError::PrematureEndOfInput),
        }
    }
}

impl<R: io::Read> io::Read for HexReader<R> {
    fn read(&mut self, buf: &mut [u8]) -> io::Result<usize> {
        let mut n_decoded = 0;
        for loc in buf.iter_mut() {
            loop {
                let mut b = [0u8; 1];
                let n_read = self.reader.read(&mut b)?;
                if n_read == 0 {
                    self.decoder.finish().map_err(DecodeError::into_io)?;
                    return Ok(n_decoded);
                }
                if let Some(octet) = self.decoder.push(b[0]).map_err(DecodeError::into_io)? {
                    *loc = octet;
                    n_decoded += 1;
                    break;
                }
            }
        }
        Ok(n_decoded)
    }
}

pub fn decode_buf<T: ?Sized + AsRef<[u8]>>(
    input: &T,
    buf: &mut Vec<u8>,
) -> Result<usize, DecodeError> {
    let mut decoder = HexDecoder::new();
    let mut n_decoded = 0;
    for octet in input.as_ref() {
        if let Some(decoded) = decoder.push(*octet)? {
            buf.push(decoded);
            n_decoded += 1;
        }
    }
    decoder.finish()?;
    Ok(n_decoded)
}

pub fn decode<T: ?Sized + AsRef<[u8]>>(input: &T) -> Result<Vec<u8>, DecodeError> {
    let mut buffer = Vec::new();
    decode_buf(input, &mut buffer)?;
    Ok(buffer)
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::io::Read;

    #[test]
    fn decode_buf_smoke() {
        let mut decoded = vec![];
        decode_buf("cafe101b", &mut decoded).unwrap();
        assert_eq!(decoded, vec![0xca, 0xfe, 0x10, 0x1b]);

        decoded.clear();
        decode_buf("cafe 101 b", &mut decoded).unwrap();
        assert_eq!(decoded, vec![0xca, 0xfe, 0x10, 0x1b]);

        decoded.clear();
        decode_buf("cafe 101 # Comments are ignored\nb", &mut decoded).unwrap();
        assert_eq!(decoded, vec![0xca, 0xfe, 0x10, 0x1b]);
    }

    #[test]
    fn hex_reader_smoke() {
        let mut reader = HexReader::new(io::Cursor::new("cafe101b"));
        let mut buffer = Vec::new();
        reader.read_to_end(&mut buffer).expect("reading failed");
        assert_eq!(buffer, vec![0xca, 0xfe, 0x10, 0x1b]);
    }
}
