use std::{
    collections::HashMap,
    ffi::OsStr,
    fs::{self, File},
    io::BufReader,
    path::{Path, PathBuf},
};

use anyhow::{anyhow, bail, Context};
use bmx::chex;
use lexpr::{datum, Datum};

#[derive(Debug)]
struct InlineTest {
    model: bmx::Forest,
    decode: Vec<DecodeTest>,
    encode: Vec<EncodeTest>,
}

#[derive(Debug)]
struct DecodeTest {
    root_name: bmx::Ident,
    input: Vec<u8>,
    expected: Vec<Datum>,
    symmetric: bool,
}

#[derive(Debug)]
struct EncodeTest {
    path: bmx::Path,
    input: Vec<HashMap<bmx::Ident, lexpr::Value>>,
    expected: Vec<u8>,
}

impl InlineTest {
    fn from_datum(datum: datum::Ref<'_>) -> anyhow::Result<Self> {
        let mut items = datum
            .list_iter()
            .ok_or_else(|| anyhow!("expected a list"))?;
        let op = items.next().ok_or_else(|| anyhow!("list too short"))?;
        match op
            .value()
            .as_symbol()
            .ok_or_else(|| anyhow!("expected operation"))?
        {
            "test" => {}
            other => bail!("unsupported operation '{}'", other),
        }
        let mut model = None;
        let mut decode = vec![];
        let mut encode = vec![];
        for item in items {
            let mut phrase = item.list_iter().ok_or_else(|| anyhow!("expected a list"))?;

            let tag = phrase.next().ok_or_else(|| anyhow!("list too short"))?;
            match tag
                .value()
                .as_symbol()
                .ok_or_else(|| anyhow!("expected tag"))?
            {
                "model" => {
                    let mut forest = bmx::Forest::new();
                    for p in phrase {
                        bmx::parse_into_forest(&mut forest, p)?;
                    }
                    model = Some(forest);
                }
                "decode" => {
                    decode.push(DecodeTest::from_items(phrase)?);
                }
                "encode" => {
                    encode.push(EncodeTest::from_items(phrase)?);
                }
                other => bail!("unsupported tag '{}'", other),
            }
        }
        if let Some(model) = model {
            Ok(InlineTest {
                model,
                decode,
                encode,
            })
        } else {
            bail!("missing 'model'");
        }
    }

    fn run(&self) -> Vec<anyhow::Result<()>> {
        self.decode
            .iter()
            .map(|decode_test| decode_test.run(&self.model))
            .chain(
                self.encode
                    .iter()
                    .map(|encode_test| encode_test.run(&self.model)),
            )
            .collect()
    }
}

impl DecodeTest {
    fn from_items(items: datum::ListIter<'_>) -> anyhow::Result<DecodeTest> {
        let mut items = items.peekable();
        let root = items
            .next()
            .ok_or_else(|| anyhow!("premature end of 'decode' clause"))?
            .value()
            .as_symbol()
            .ok_or_else(|| anyhow!("invalid root; needs to be a symbol"))?;
        let mut input = Vec::new();
        let mut symmetric = false;
        while let Some(item) = items.peek() {
            match item.as_symbol() {
                Some("=>") => break,
                Some("<=>") => {
                    symmetric = true;
                    break;
                }
                _ => {}
            }
            let input_str = item
                .value()
                .as_str()
                .ok_or_else(|| anyhow!("input field of 'decode' clause must be a string"))?;
            chex::decode_buf(input_str, &mut input).context("expected base16 string")?;
            items.next().expect("peek method broken");
        }
        if items.peek().is_none() {
            bail!("premature end of 'decode' clause, expected `=>`");
        }
        let expected = items.skip(1).map(Datum::from).collect();
        Ok(DecodeTest {
            root_name: root.into(),
            input,
            expected,
            symmetric,
        })
    }

    fn run(&self, forest: &bmx::Forest) -> anyhow::Result<()> {
        // TODO: this is mostly-duplicated from `run_test`
        let decoder = forest.decoder(&self.root_name)?;
        let mut bit_pos = 0;
        let mut expected_iter = self.expected.iter();
        let mut item_idx = 0;
        while let Some((decoded, n_bits)) = decoder.decode(&self.input, bit_pos, forest)? {
            let expected = expected_iter
                .next()
                .ok_or_else(|| anyhow!("decoded more items than expected"))?;
            let value: lexpr::Value = forest.resolve(&decoder, &decoded).into();
            if expected.value() != &value {
                bail!(
                    "mismatch: item {} (at bit {}) should decode as {}, got {}",
                    item_idx + 1,
                    bit_pos,
                    expected.value(),
                    value
                );
            }
            bit_pos += n_bits;
            item_idx += 1;
        }
        if expected_iter.next().is_some() {
            bail!("expected more items");
        }
        if bit_pos < self.input.len() * 8 {
            let n_trailing = self.input.len() * 8 - bit_pos;
            if n_trailing > 0 {
                bail!("{} bits of trailing garbage detected", n_trailing);
            }
        }
        if self.symmetric {
            let encode_test = EncodeTest {
                path: [self.root_name.clone()].iter().collect(),
                input: self
                    .expected
                    .iter()
                    .map(|input| alist_hashmap(input.as_ref()))
                    .collect::<Result<_, _>>()?,
                expected: self.input.clone(),
            };
            encode_test.run(forest)?;
        }
        Ok(())
    }
}

impl EncodeTest {
    fn from_items(mut items: datum::ListIter<'_>) -> anyhow::Result<EncodeTest> {
        let path: bmx::Path = items
            .next()
            .ok_or_else(|| anyhow!("premature end of 'encode' clause"))?
            .value()
            .as_symbol()
            .ok_or_else(|| anyhow!("invalid node; needs to be a path"))?
            .into();
        let fields = items
            .next()
            .ok_or_else(|| anyhow!("premature end of 'encode' clause, expected field alist"))?;
        let separator = items
            .next()
            .ok_or_else(|| anyhow!("premature end of 'encode' clause, expected `=>`"))?;
        if separator.as_symbol() == Some("=>") {
            bail!("expected `=>`, got {}", separator.value());
        }
        let mut expected = Vec::new();
        for item in items {
            let expected_str = item
                .value()
                .as_str()
                .ok_or_else(|| anyhow!("input field of 'decode' clause must be a string"))?;
            chex::decode_buf(expected_str, &mut expected).context("expected base16 string")?;
        }
        Ok(EncodeTest {
            path,
            expected,
            input: vec![alist_hashmap(fields)?], // TODO: multiple field alists
        })
    }

    fn run(&self, forest: &bmx::Forest) -> anyhow::Result<()> {
        let encoder = forest.encoder(&self.path)?;
        let mut output = Vec::new();
        for input in &self.input {
            output.extend(encoder.encode(input)?);
        }
        if output != self.expected {
            bail!(
                "unexpected output: `{}`, expected `{}`",
                hex::encode(&output),
                hex::encode(&self.expected),
            );
        }
        Ok(())
    }
}

fn alist_hashmap(alist: datum::Ref<'_>) -> anyhow::Result<HashMap<bmx::Ident, lexpr::Value>> {
    alist
        .list_iter()
        .ok_or_else(|| anyhow!("expected field alist, got {}", alist.value()))?
        .map(|item| {
            let (car, cdr) = item
                .as_pair()
                .ok_or_else(|| anyhow!("expected field alist item, got {}", item.value()))?;
            Ok((
                car.as_symbol().map(bmx::Ident::from).ok_or_else(|| {
                    anyhow!("expected field alist key symbol, got {}", car.value())
                })?,
                cdr.value().clone(),
            ))
        })
        .collect()
}

fn execute_tests(path: &Path) -> anyhow::Result<Vec<anyhow::Result<()>>> {
    let file = File::open(path)
        .with_context(|| format!("could not open testcase file '{}'", path.display()))?;
    let mut parser = lexpr::Parser::from_reader(BufReader::new(file));
    let mut results = Vec::new();
    for datum in parser.datum_iter() {
        let test = datum
            .map_err(anyhow::Error::from)
            .and_then(|d| InlineTest::from_datum(d.as_ref()))
            .with_context(|| format!("syntax error in test case '{}'", path.display()))?;
        results.extend(test.run());
    }
    Ok(results)
}

fn main() {
    env_logger::init();

    use std::collections::BTreeMap;

    let mut succeeded_suites = BTreeMap::new();
    let mut failed_suites = BTreeMap::new();

    let test_files: Vec<_> = std::env::args_os().skip(1).map(PathBuf::from).collect();
    let test_files = if test_files.is_empty() {
        fs::read_dir("test-data")
            .expect("could not read `test-data` directory")
            .map(|entry| {
                entry
                    .expect("error reading `test-data` directory contents")
                    .path()
            })
            .filter(|path| Some(OsStr::new("tests")) == path.extension())
            .collect()
    } else {
        test_files
    };
    for path in test_files {
        match execute_tests(&path) {
            Ok(results) => {
                succeeded_suites.insert(path, results);
            }
            Err(e) => {
                failed_suites.insert(path, e);
            }
        }
    }
    if !failed_suites.is_empty() {
        println!(
            "Of {} test suites, {} failed to parse:",
            succeeded_suites.len() + failed_suites.len(),
            failed_suites.len()
        );
        for (path, error) in &failed_suites {
            eprintln!("{}: {}", path.display(), error);
            for cause in error.chain().skip(1) {
                eprintln!("  Caused by {}", cause);
            }
        }
    }
    let mut n_err_total = 0;
    let mut n_tests_total = 0;
    for (path, results) in succeeded_suites.into_iter() {
        n_tests_total += results.len();
        for error in results.into_iter().filter_map(|r| r.err()) {
            eprintln!("{}: {}", path.display(), error);
            for cause in error.chain().skip(1) {
                eprintln!("  Caused by {}", cause);
            }
            n_err_total += 1;
        }
    }
    if !failed_suites.is_empty() {
        let failed: Vec<_> = failed_suites.keys().map(|p| p.to_string_lossy()).collect();
        panic!("Could not run suites: {}", failed.join(", "));
    }
    if n_err_total > 0 {
        panic!(
            "Total errors in ran suites: {}/{}",
            n_err_total, n_tests_total
        );
    }
}
