# bmx - Binary modelling expressions

`bmx` allows matching binary data against a tree-shaped description of
the expected data format. The description of the data, referred to as
"grammar tree" below, has an external representation that is intended
to be compact, yet easy to produce and consume for humans as well as
machines.

The state of `bmx` is currently embryonic and highly experimental, or
put more succinctly and buzzwordly, it currently a PoC. The (already
working) example below will get you an idea of its purpose, bear in
mind that at the moment, the code is just a rough skeleton.

For example you could describe the instructions for a simple Logo-like
binary language like this:

```scheme
(root Command
  (fields (tag u8 hidden)))

(branch Command.PencilDown
  (condition (= tag 0)))

(branch Command.PencilUp
  (condition (= tag 1)))

(branch Command.Turn
  (condition (= tag 2))
  (fields (angle u32)))

(branch Command.Move
  (condition (= tag 3))
  (fields (amount u32)))

(branch Command.Loop
  (condition (= tag 4))
  (fields (n-iterations u32)
          ;; TODO: Allow command list
          (command Command)))
```

Giving `bmx` the above grammar, you can feed it arbitrary input data
to decode. For example, the following data, given in a hex-encoded,
whitespace-ignoring format:

```
00
02 00002710
03 00000710
01
00
04 00000004
02 0000003c
01
```

Will be decoded as follows:

```
% cargo run -- -l examples/simple.scm -f chex < examples/simple.chex
[...]
decoded 8 bits at 0 as PencilDown
decoded 40 bits at 8 as Turn { angle: 10000 }
decoded 40 bits at 48 as Move { amount: 1808 }
decoded 8 bits at 88 as PencilUp
decoded 8 bits at 96 as PencilDown
decoded 80 bits at 104 as Loop { n-iterations: 4, command: Turn { angle: 60 } }
decoded 8 bits at 184 as PencilUp
```
