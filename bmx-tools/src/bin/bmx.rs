#![warn(rust_2018_idioms)]

use std::{
    fmt, fs,
    io::{self, BufReader, Read},
    path::PathBuf,
};

use bmx::chex::HexReader;
use clap::Parser;
use miette::{bail, Diagnostic};
use miette::{Context, IntoDiagnostic};
use thiserror::Error;

#[derive(Debug, Copy, Clone)]
enum InputFormat {
    Binary,
    CommentedHex,
}

impl Default for InputFormat {
    fn default() -> Self {
        InputFormat::Binary
    }
}

impl std::str::FromStr for InputFormat {
    type Err = InvalidInputFormat;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        use InputFormat::*;
        match s {
            "bin" => Ok(Binary),
            "chex" => Ok(CommentedHex),
            _ => Err(InvalidInputFormat),
        }
    }
}

impl fmt::Display for InputFormat {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        use InputFormat::*;
        match self {
            Binary => f.write_str("bin"),
            CommentedHex => f.write_str("chex"),
        }
    }
}

#[derive(Debug)]
struct InvalidInputFormat;

impl fmt::Display for InvalidInputFormat {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, r#"invalid input format ("bin" and "chex" are accepted)"#)
    }
}

impl std::error::Error for InvalidInputFormat {}

#[derive(Parser)]
#[command(about = "Binary model expression decoder", long_about = None)]
struct Opt {
    #[arg(short, long, help = "Models to load")]
    load: Vec<PathBuf>,
    #[arg(short, long, help = "Name of decoding root")]
    root: Option<String>,
    #[arg(
        short = 'f',
        long = "from",
        default_value_t,
        help = "Input data format"
    )]
    input_format: InputFormat,
}

#[derive(Error, Debug, Diagnostic)]
#[error("could not load source files")]
struct LoadingError {
    #[related]
    errors: Vec<miette::Report>,
}

fn main() -> miette::Result<()> {
    env_logger::init();

    let opt = Opt::parse();
    let mut forest = bmx::Forest::new();
    let load_errors: Vec<_> = opt
        .load
        .iter()
        .filter_map(|path| {
            let source = match fs::read_to_string(path)
                .into_diagnostic()
                .with_context(|| format!("error reading file `{}`", path.display()))
            {
                Ok(source) => source,
                Err(e) => return Some(e),
            };
            bmx::read_into_forest(&mut forest, &mut source.as_bytes())
                .err()
                .map(|e| {
                    miette::Report::new(
                        e.diagnostic(&forest, &source).named(path.to_string_lossy()),
                    )
                })
        })
        .collect();

    if !load_errors.is_empty() {
        bail!(LoadingError {
            errors: load_errors
        })
    }

    let root_name: bmx::Ident = match &opt.root {
        Some(name) => name.into(),
        None => match forest.root_names().len() {
            0 => bail!("no decoding roots defined"),
            1 => forest.root_names().next().unwrap().clone(),
            _ => bail!("no decoding root specfied, and multiple available"),
        },
    };
    let decoder = forest
        .decoder(&root_name)
        .into_diagnostic()
        .with_context(|| format!("could not prepare root `{root_name}`"))?;
    let mut input = Vec::with_capacity(1024);
    match opt.input_format {
        InputFormat::Binary => {
            io::stdin()
                .read_to_end(&mut input)
                .into_diagnostic()
                .context("reading from stdin failed")?;
        }
        InputFormat::CommentedHex => {
            let mut reader = HexReader::new(BufReader::new(io::stdin()));
            reader
                .read_to_end(&mut input)
                .into_diagnostic()
                .context("reading from stdin failed")?;
        }
    }
    let mut bit_pos = 0;
    while let Some((decoded, n_bits)) =
        decoder.decode(&input, bit_pos, &forest).into_diagnostic()?
    {
        let record = forest.resolve(&decoder, &decoded);
        println!("decoded {} bits at {} as {}", n_bits, bit_pos, record,);
        bit_pos += n_bits;
    }
    if bit_pos < input.len() * 8 {
        let n_trailing = input.len() * 8 - bit_pos;
        println!("{} bits of trailing garbage detected", n_trailing);
    }
    Ok(())
}
